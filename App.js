import React, {Component} from 'react';
import {View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import HomeScreen from './src/components/Home';
import Chapter1 from './src/Chapters/NutritionInPlants';
import Chapter2 from './src/Chapters/NutritionInAnimals';
import Chapter3 from './src/Chapters/FibreToFabric';
import Chapter4 from './src/Chapters/Heat';
import Chapter5 from './src/Chapters/AcidsBase';
import Chapter6 from './src/Chapters/PhysicalAndChemicalChanges';
import Chapter7 from './src/Chapters/WeatherClimate';
import Chapter8 from './src/Chapters/WindsStorm';
import Chapter9 from './src/Chapters/Soil';
import Chapter10 from './src/Chapters/Respiration';
import Chapter11 from './src/Chapters/Transport';
import Chapter12 from './src/Chapters/ReproductionInPlants';
import Chapter13 from './src/Chapters/MotionAndTime';
import Chapter14 from './src/Chapters/ElectricCurrent';
import Chapter15 from './src/Chapters/Light';
import Chapter16 from './src/Chapters/Water';
import Chapter17 from './src/Chapters/Forests';
import Chapter18 from './src/Chapters/WastewaterStory';
import DrawerItem from './src/components/DrawerItem';

import {BannerAd, BannerAdSize, TestIds} from '@react-native-firebase/admob';

const BANNER_ID = 'ca-app-pub-5928451080551316/3214690974';

class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen
            name="Home"
            component={MyDrawer}
            headerMode="none"
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Chapter1"
            component={Chapter1}
            options={{headerTitle: 'Nutrition in Plants'}}
          />
          <Stack.Screen
            name="Chapter2"
            component={Chapter2}
            options={{headerTitle: 'Nutrition in Animals'}}
          />
          <Stack.Screen
            name="Chapter3"
            component={Chapter3}
            options={{headerTitle: 'Fibre to Fabric'}}
          />
          <Stack.Screen
            name="Chapter4"
            component={Chapter4}
            options={{headerTitle: 'Heat'}}
          />
          <Stack.Screen
            name="Chapter5"
            component={Chapter5}
            options={{headerTitle: 'Acids, Bases and Salts'}}
          />
          <Stack.Screen
            name="Chapter6"
            component={Chapter6}
            options={{headerTitle: 'Physical and Chemical Changes'}}
          />
          <Stack.Screen
            name="Chapter7"
            component={Chapter7}
            options={{headerTitle: 'Weather, Climate And . . .'}}
          />
          <Stack.Screen
            name="Chapter8"
            component={Chapter8}
            options={{headerTitle: 'Winds, Storms And Cyclones'}}
          />
          <Stack.Screen
            name="Chapter9"
            component={Chapter9}
            options={{headerTitle: 'Soil'}}
          />
          <Stack.Screen
            name="Chapter10"
            component={Chapter10}
            options={{headerTitle: 'Respiration in Organisms'}}
          />
          <Stack.Screen
            name="Chapter11"
            component={Chapter11}
            options={{headerTitle: 'Transport in Animals and Plants'}}
          />
          <Stack.Screen
            name="Chapter12"
            component={Chapter12}
            options={{headerTitle: 'Reproduction in Plants'}}
          />
          <Stack.Screen
            name="Chapter13"
            component={Chapter13}
            options={{headerTitle: 'Motion and Time'}}
          />
          <Stack.Screen
            name="Chapter14"
            component={Chapter14}
            options={{headerTitle: 'Electric Currents and Its Effects'}}
          />
          <Stack.Screen
            name="Chapter15"
            component={Chapter15}
            options={{headerTitle: 'Light'}}
          />
          <Stack.Screen
            name="Chapter16"
            component={Chapter16}
            options={{headerTitle: 'Water: A Precious Resource'}}
          />
          <Stack.Screen
            name="Chapter17"
            component={Chapter17}
            options={{headerTitle: 'Forests : Our Lifeline'}}
          />
          <Stack.Screen
            name="Chapter18"
            component={Chapter18}
            options={{headerTitle: 'Wastewater Story'}}
          />
        </Stack.Navigator>
        <View style={{width: '100%'}}>
          {/* Banner */}
          <BannerAd
            //unitId={TestIds.BANNER}
            unitId={BANNER_ID}
            size={BannerAdSize.SMART_BANNER}
            requestOptions={{
              requestNonPersonalizedAdsOnly: true,
            }}
            onAdLoaded={() => {
              console.log('Advert loaded');
            }}
            onAdFailedToLoad={error => {
              console.error('Advert failed to load: ', error);
            }}
          />
        </View>
      </NavigationContainer>
    );
  }
}

function MyDrawer() {
  return (
    <Drawer.Navigator drawerContent={props => <DrawerItem />}>
      <Drawer.Screen name="HomeScreen" component={HomeScreen} />
    </Drawer.Navigator>
  );
}

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
export default App;
