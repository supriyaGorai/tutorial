import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  StatusBar,
  Text,
  Share,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import Icon1 from 'react-native-vector-icons/AntDesign';

class DrawerItem extends Component {
  Share = async () => {
    try {
      const result = await Share.share({
        message: `Share With Friends : https://play.google.com/store`,
        title: 'Share With Friends',
        url: 'https://play.google.com/store',
      });

      if (result.action === Share.sharedAction) {
        console.log('Post Shared');
      } else if (result.action === Share.dismissedAction) {
        // dismissed
        console.log('Post cancelled');
      }
    } catch (error) {
      alert(error.message);
    }
  };

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <StatusBar barStyle="dark-content" />
        <View style={styles.container}>
          <View style={styles.heading}>
            <Icon1 name="book" size={30} color="#fff" style={styles.icon} />
            <Text style={{fontSize: 25, color: '#fff'}}>
              Lakhmir Singh Manjit Kaur Class 7 Science Solution
            </Text>
          </View>

          <View style={styles.body}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon1 name="rightcircle" size={30} color="#3F51B5" />
              <Text style={styles.text}>Class - 7 (seven)</Text>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon1 name="rightcircle" size={30} color="#3F51B5" />
              <Text style={styles.text}>Subject - Science</Text>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon1 name="rightcircle" size={30} color="#3F51B5" />
              <Text style={styles.text}>Board - CBSE</Text>
            </View>
          </View>

          <View style={styles.btnDiv}>
            <TouchableOpacity
              style={styles.shareBtn}
              onPress={() => this.Share()}>
              <Icon name="share" size={30} color="#586776" />
              <Text style={{fontSize: 22, marginLeft: 3, color: '#586776'}}>
                Share App
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  heading: {
    backgroundColor: '#3F51B5',
    paddingBottom: 70,
    paddingTop: 50,
    paddingHorizontal: 20,
    alignItems: 'center',
    flex: 1,
  },
  icon: {
    borderWidth: 2,
    borderColor: '#fff',
    padding: 20,
    borderRadius: 50,
    marginBottom: 15,
  },
  body: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 40,
    borderBottomWidth: 1,
    borderBottomColor: '#c1c1c1',
  },
  text: {
    fontSize: 20,
    color: '#3F51B5',
    margin: 10,
  },
  btnDiv: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  shareBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default DrawerItem;
