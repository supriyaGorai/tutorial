import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import {Header, Left, Body, Button, Icon, Title} from 'native-base';
import {
  TestIds,
  InterstitialAd,
  AdEventType,
} from '@react-native-firebase/admob';

//InterstitialAd
const INTERSTIAL_ID = 'ca-app-pub-5928451080551316/3489332458';

const interstitial = InterstitialAd.createForAdRequest(INTERSTIAL_ID, {
  requestNonPersonalizedAdsOnly: true,
});
interstitial.onAdEvent(type => {
  if (type === AdEventType.LOADED) {
    interstitial.show();
  }
});

class Home extends Component {
  navigateTOChapter = chapterName => {
    var RandomNumber = Math.floor(Math.random() * 100) + 1;
    if (RandomNumber % 5 === 0) {
      interstitial.load();
    }
    this.props.navigation.navigate(chapterName);
  };

  render() {
    const logo = require('./../../assets/book-logo.png');
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <StatusBar barStyle="dark-content" />

        <Header
          style={{backgroundColor: '#fff'}}
          androidStatusBarColor="#3F51B5">
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.openDrawer()}>
              <Icon name="menu" style={{color: '#000', fontSize: 30}} />
            </Button>
          </Left>
          <Body style={{marginRight: '15%'}}>
            <Title style={{color: '#000', fontSize: 18}}>
              Class 7 Science Solutions
            </Title>
          </Body>
        </Header>

        <View style={styles.headingTextView}>
          <Text style={styles.headingtext}>Chapters</Text>
        </View>
        <ScrollView style={{flex: 1}}>
          <View style={styles.container}>
            {/* 1 Nutrition in Plants */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter1')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Nutrition in Plants</Text>
            </TouchableOpacity>
            {/* 2 Nutrition in Animals */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter2')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Nutrition in Animals</Text>
            </TouchableOpacity>
            {/* 3 Fibre to Fabric */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter3')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Fibre to Fabric</Text>
            </TouchableOpacity>
            {/* 4 Heat */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter4')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Heat</Text>
            </TouchableOpacity>
            {/* 5 Acids, Bases and Salts */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter5')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Acids, Bases and Salts</Text>
            </TouchableOpacity>
            {/* 6 Physical and Chemical Changes */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter6')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>
                Physical and Chemical Changes
              </Text>
            </TouchableOpacity>
            {/* 7 Weather, Climate And Adaptations of Animals to Climate */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter7')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>
                Weather, Climate And Adaptations of Animals to Climate
              </Text>
            </TouchableOpacity>
            {/* 8 Winds, Storms And Cyclones */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter8')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Winds, Storms And Cyclones</Text>
            </TouchableOpacity>
            {/* 9  Soil*/}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter9')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Soil</Text>
            </TouchableOpacity>
            {/* 10  Respiration in Organisms*/}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter10')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Respiration in Organisms</Text>
            </TouchableOpacity>
            {/* 11 Transport in Animals and Plants */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter11')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>
                Transport in Animals and Plants
              </Text>
            </TouchableOpacity>
            {/* 12 Reproduction in Plants */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter12')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Reproduction in Plants</Text>
            </TouchableOpacity>
            {/* 13 Motion and Time */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter13')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Motion and Time</Text>
            </TouchableOpacity>
            {/* 14 Electric Currents and Its Effects */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter14')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>
                Electric Currents and Its Effects
              </Text>
            </TouchableOpacity>
            {/* 15 Light */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter15')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}> Light</Text>
            </TouchableOpacity>
            {/* 16 Water: A Precious Resource */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter16')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Water: A Precious Resource</Text>
            </TouchableOpacity>
            {/* 17 Forests : Our Lifeline */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter17')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Forests : Our Lifeline</Text>
            </TouchableOpacity>
            {/* 18 Wastewater Story */}
            <TouchableOpacity
              style={styles.chapterButton}
              onPress={() => this.navigateTOChapter('Chapter18')}>
              <Image source={logo} style={styles.logo} />
              <Text style={styles.chapterName}>Wastewater Story</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  headingTextView: {
    //backgroundColor: "#EAF0F1",
    paddingVertical: 15,
  },
  headingtext: {
    fontSize: 20,
    textAlign: 'center',
  },
  chapterButton: {
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#fafa',
    width: '90%',
  },
  logo: {
    height: 40,
    width: 40,
  },
  chapterName: {
    fontSize: 18,
    marginLeft: 5,
  },
});

export default Home;
