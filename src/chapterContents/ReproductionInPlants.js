const htmlContent =`<h1><span style="color: #000080;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 12th Chapter <span style="color: #ff0000;">Reproduction In Plants</span> Solutions</strong></span></h1>
Lakhmir Singh and Manjit Kaur Science solution: Reproduction In Plants Chapter 12. Here you get easy solutions of Lakhmir Singh and Manjit Kaur Science solution Chapter 12. Here we give Chapter 12 all solution of class 7. Its help you to complete your homework.

Board – CBSE

Text Book - SCIENCE

Class – 7
<h2 style="text-align: center;"><span style="color: #800080;"><strong>Chapter – 12</strong></span></h2>
<h3><span style="color: #ff0000;"><strong>Very Short answer Type Questions Answers:</strong></span></h3>
<div>(1) Rose.

</br>(2) Potato.

</br>(3) Sweet Potato and Dahlia.

</br>(4) Bryophyllum.

</br>(5) Bread mould plant.

</br>(6) yeast

</br>(7) Vegetative propagation by Stems.

</br>(8) Budding.

</br>(9) Fragmentation.

</br>(10) Spore formation.

</br>(11) (a) Sexual Reproduction

</br>(b) Asexual reproduction.

</br>(12) (a) Sexual Reproduction

</br>(b) Asexual reproduction.

</br>(13) Paddy, Wheat.

</br>(14) Rose, Brophyllum.

</br>(15) (a) Yeast

</br>(b) Bread Mould.

</br>(16) Coconut.

</br>(17) Xanthium.

</br>(18) In Flower Plants reproductive organ located.

</br>(19) (a) Stamen

</br>(b) Pistil.

</br>(20) Drumstick, Maple.

</br>(21) Madar, Sunflower.

</br>(22) Grasses.

</br>(23) (a) Pollen- Male part of plant.

</br>(b) Ovules- female part of plant

</br>(24) Pollen grains.

</br>(25) (a) Male Gamets or Male Sex Cells.

</br>(b) Female Gamets or Female Sex Cells.

</br>(26) (a) Pollination

</br>(b) Fertilisation.

</br>(27) Zygote

</br>(28) Ovary

</br>(29) (a) False

</br>(b) True

</br>(30) (a) Unisexual

</br>(b) Pollination

</br>(c) Fertilisation

</br>(d) Female; Male.

</br>(e) Seed; Fruit.

</br>(f) Reproductive

</br>(g) Sexual

</br>(h) Zygote

</br>(i) Zygote

</br>(j) Embryo

</br>(k) Root; Stem.

</br>(l) Wind; Water; Animal

</br>(m) Animal; Water.
<h3><span style="color: #ff0000;"><strong>Short answer Type Questions Answers:</strong></span></h3>
</br>(31) (a) Three agents which can carry out pollination are Insects, Wind, and Water.

</br>(b) Three agents are wind, water, animals.

</br>(32) The transfer of pollen grains from the anther of a stamen to the stigma of a pistil is called pollination.

</br>Pollination can occur in two ways- (i) Self Pollination (ii) Cross Pollination.

</br>(33)
<table>
<tbody>
<tr>
<td width="319"><strong>Column I</strong></td>
<td width="319"><strong>Column II</strong></td>
</tr>
<tr>
<td width="319">(i) Bud</td>
<td width="319">(c) Yeast</td>
</tr>
<tr>
<td width="319">(ii) Eyes</td>
<td width="319">(e) Potato</td>
</tr>
<tr>
<td width="319">(iii) Fragmentation</td>
<td width="319">(b) Spirogyra</td>
</tr>
<tr>
<td width="319">(iv) Wings</td>
<td width="319">(a) Maple</td>
</tr>
<tr>
<td width="319">(v) Spores</td>
<td width="319">(d) Bread Mould</td>
</tr>
</tbody>
</table>
</br>(34) (a) The flowers which contain only one reproductive organ are called unisexual flowers.

The flowers which contain both the reproductive organs are called bisexual flowers.

</br>(a) Two plants which have Unisexual flowers:- Papaya, Watermelon.

</br>(b) Two plants which have bisexual flowers:- rose, mustard.

</br>(35) The seed contains embryo and food for developing new plant.

Two plants are castor and balsam.

</br>(36) The vegetative parts of a plant are Stem, Roots, Leaves.

</br>(37) The breaking up of the body of a plant into two or more pieces on maturing each of which subsequently grows to form a new plant is called fragmentation.

One plant which reproduces through fragmentation is Algae.

</br>(38) Bryophyllum plants can be reproduced from its leaves.

</br>(39) A cut part of a potato with a bud can grow into a new potato plant.

</br>(40) Two advantages of vegetative propagation in plants are:-

</br>(a) The new plants produced by vegetative propagation take much less time to grow and bear flowers and fruits as compared to the plants grown from seeds.

</br>(b) The new plants produced by vegetative propagation are exactly like the parent plant.

</br>(41) Plants such as ferns and mosses reproduce by means of spores.

</br>(42) Stamen is the male reproductive organ of the plant.

Pistil is the female reproductive organ of the plant.

</br>(43) The difference between self pollination and cross pollination are:-

(a) When the pollen grains from the anther of a flower are transferred to the stigma of the same flower it is called self pollination. Whereas

When the pollen grains from the anther of a flower on one plant are transferred to the stigma of a flower on another similar plant it is called cross pollination.

</br>(44) The insects carry pollen from flower to flower and help in pollination.

</br>(45)

Fertilization takes place after pollination. Afterthat the pollen grain develops a tube called pollen tube which carriers male germ cells to the ovary. The male germ cells fuses with the female germ cell in the ovary and forms zygote. The Zygote further forms the embryo. The embryo develops a protective coating and forms seed. Then the ovary forms a fruit with seed in it.

</br>(46) The process in which the male gamete present in pollen grain fuses (joins) with the female gamete present in ovule to form a new cell called zygote is called fertilization.

<u>How the Process of fertilization take place in flowers:-</u>

When pollen falls on stigma, it germinates and gives rise to a pollen tube that passes through the style and reaches the ovary of a pistil.

When the pollen tube reaches an ovule, it releases the male gametes. A male gamete fuses with a female gamete in the ovule. The cell which is formed after the fusion of a male and a female gamete is known as zygote.

This zygote divides several times in order to form the embryo present inside the seed. In this way the process of fertilization take place in flowers.

</br>(47) The sexual reproduction in plants involves the combination of male and female gametes of the plant which leads to the formation of the seeds of the plant. These seeds can be used to grow new plants.

</br>(48) The seeds or fruits which are dispersed by wind either have wing like structures or having air or they are very small and weightless.

Five’s are:- Drumstick; Maple; Madar; Sunflower, Cotton.

</br>(49) (a) Drumstick- By wind.

</br>(b) Xanthium- By Animal.

</br>(c) Castor- By an Explosive Mechanism.

</br>(d) Maple- By wind.

</br>(50) If all the seeds of a plant were to fall at the same place below the plant and grow there therefore overcrowding will happened as well as plants cant be grow properly as they are not getting enough water and minerals for being at the same place.

</br>(51) This seeds or fruits which are dispersed by water develop ‘floating ability’ in the form of spongy or fibrous outer coats.

Two Example:- Water lily plant and Coconut.

</br>(52)
<table>
<tbody>
<tr>
<td width="319">(a) Wings</td>
<td width="319">Maple</td>
</tr>
<tr>
<td width="319">(b) Hair</td>
<td width="319">Sunflower</td>
</tr>
<tr>
<td width="319">(c) Fibrous coat</td>
<td width="319">Coconut</td>
</tr>
<tr>
<td width="319">(d) Hooks</td>
<td width="319">Urena</td>
</tr>
<tr>
<td width="319">(e) Property of bursting</td>
<td width="319">Balsam</td>
</tr>
</tbody>
</table>
</br>(53) This fruits have fibrous outer coat which enables them to float in  water and carried away by flowing water to far off places.

</br>(54) This seeds or fruits having hooks on their surface. That’s why they get attached to the hairy bodies of the passing animals and carried away to distant place.

Example: Xanthium; Urena.

</br>(55) (a) Madar (Aak)- Hairy seed.

</br>(b) Sunflower- Seed.

</br>(c) Urena- Animals.

</br>(d) Balsam- Explosive Mechanism.
<h3><span style="color: #ff0000;"><strong>Long Answer Type Questions Answers:</strong></span></h3>
<strong>(56) <u>Function Of Flower:- </u></strong>The function of a flower is to make male and female gametes and to ensure that fertilization will take place to make seeds for growing new plants.

<strong><u>Flowers Generally So colorful and fragrant:</u></strong>

Flowers are the reproductive part of plant. They need vector which to transfer their pollen grains . So, for attracting insects , animals , human they are so colourful.

If they are not so colourful and fragrant and sweet smelling then no one attracts towards flower and the pollen grains are not transfer from one to another flower and then the flower are not able to show reproduction. If there is no reproduction then there is no fruit formation in plant. And This reason it generally so colorful and fragrant.

</br>(57) (a) Reproduction:- The production of new organisms from the existing organisms of the same species is called reproduction.

Two type of methods of reproduction in plants are Sexual and Asexual reproduction.

</br>(b) Vegetative Propagation:- In vegetative propagation new plants are produced from the parts of old plants like stems, roots or leaves.

Two plants are Potato, Bryophyllum.

</br>(58) (a) <u>Asexual Reproduction:-</u>  The production of new plants from existing plants without the involvement of gametes.

Example: Dahlia, Sugarcane.

</br>(b) <u>Sexual Reproduction:- </u>The production of new plants from existing plants by the fusion of their gametes.

Example: Wheat, Gram.

</br>(59) (a) The difference between Asexual and Sexual Reproduction are:

The production of new plants from existing plants without the involvement of gametes called Asexual Reproduction whereas The production of new plants from existing plants by the fusion of their gametes called sexual reproduction.

</br><strong>The various method of Asexual reproduction are:-</strong>

</br>(a) Vegetative Propagation.

</br>(b) Budding

</br>(c) Fragmentation

</br>(d) Spore formation.

</br><strong>(b) cuttings method for the propagation of rose plants:</strong>

Cutting is removing a portion of the stem and fixing it in the soil to allow the growth of roots and buds growing into shoots.

In many plants stems are cut into small pieces and placed in moist soil to grow a number of plants of the same type. They strike root at the base and the adventitious roots grow up into shoots.

</br>(60) (a) Seed Dispersal means to scatter seeds over a wide area.

Seed Dispersal is beneficial because:-

</br>(i) It prevents the overcrowding of plants in an area.

</br>(ii) It prevents the competition for water, minerals and Sunlight among the same kind of plants.

</br>(b) The various ways by which seed are dispersed:

</br>(i) Dispersal of Seeds by Wind: This seeds have wing like structure or they have hair or they are very small or light which is beneficial to carried away by the wind.

</br>(ii) Dispersal of Seeds by Water:- They have spongy or fibrous coat outside, that’s why they are easily float by water.

</br>(iii) Dispersal of Seeds by Animals:- This seeds have hooks on their surface by which they get attached to the hairy bodies of the passing animals and carried away to far of places.</div>`;
export default htmlContent;