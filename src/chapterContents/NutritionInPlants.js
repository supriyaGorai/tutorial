const htmlContent = `
<div>
Lakhmir Singh Manjit Kaur Solution Class 7 Science 1st Chapter - "Nutrition in Plants" in here.
<h3><span style="color: #ff0000;"><strong>Very Short Answer Type Questions Solution:</strong></span></h3>
<p><strong>(1)</strong> Stomata</p> 
<p><strong>(2)</strong> Photosynthesis</P>
<p><strong>(3)</strong> The process by which plants make food called photosynthesis.</P>
<p><strong>(4)</strong> a-False; b-True; c-True; d-False; e- False.</P>
<p><strong>(5)</strong> Pea plant.</P>
<p><strong>(6)</strong> Algae.</P>
<p><strong>(7)</strong> The algae are green because they have chlorophyll.</P>
<p><strong>(8)</strong> a- Carbon-Di-Oxide; b- Oxygen.</P>
<p><strong>(9)</strong> a- Autotrophs; b- Heterotrophs</P>
<p><strong>(10)</strong> Symbiotic relationship.</P>
<p><strong>(11)</strong> Symbiotic relationship.</P>
<p><strong>(12)</strong> Leaves.</P>
<p><strong>(13)</strong> The leaves of plant usually green because they have chlorophyll.</P>
<p><strong>(14)</strong> Chlorophyll.</P>
<p><strong>(15)</strong> The three plant-nutrients that are commonly present in fertilizer and manure are:-- Nitrogen, Phosphorus and Potassium.</P>
<p><strong>(16)</strong> Rhizobium bacteria.</P>
<p><strong>(17)</strong> Symbiotic plant.</P>
<p><strong>(18)</strong> The two leguminous plants are:- Pea plant, Gram.</P>
<p><strong>(19)</strong> One autotrophic plant: Mango. One heterotrophic plant: Mushroom.</P>
<p><strong>(20)</strong> Cascuta plant.</P>
<p><strong>(21)</strong> Pitcher plant.</P>
<p><strong>(22)</strong> In Algae’s photosynthesis occurs other then leaves. It occurs at  inside plant cells in small things called chloroplasts.</P>
<p><strong>(23)</strong> Four foods made by plants which are an important part of diet- Starch, Fat, Protein, and vitamin.</P>
<p><strong>(24)</strong> The two groups are insectivorous plant and symbiotic plant.</P>
<p><strong>(25)</strong> (a) mushroom; yeast (b) solution (c) air (d) alga (e) Fungus (f) starch (g) chlorophyll (h) carbon-di-oxide; oxygen (i) oxygen (j) glucose (k) proteins (l) cells.</P>

<h3><span style="color: #ff0000;"><strong>Short Answer Type Questions Solution:</strong></span></h3>
<strong>(26) </strong>
<table>
<tbody>
<tr>
<td width="319"><strong>Column I</strong></td>
<td width="319"><strong>Column II</strong></td>
</tr>
<tr>
<td width="319">(i) Chlorophyll</td>
<td width="319">(d) leaf</td>
</tr>
<tr>
<td width="319">(ii) Nitrogen</td>
<td width="319">(a) Rhizobium bacteria</td>
</tr>
<tr>
<td width="319">(iii)</td>
<td width="319">(e) Parasite</td>
</tr>
<tr>
<td width="319">(iv) Animals</td>
<td width="319">(b) Heterotrophs</td>
</tr>
<tr>
<td width="319">(v) Insects</td>
<td width="319">(c) Pitcher plant</td>
</tr>
</tbody>
</table>
<strong>(27) The presence f starch in leaves are tested by following:</strong>

No. 1: Pluck a green leaf from a plant.

No. 2: Boil the leaf in alcohol to remove the green pigment chlorophyll from it.

No. 3: Wash the discolourished leaf with water to remove any chlorophyll sticking to it.

No. 4: Pour dilute iodine solution from a dropper over the discolourished leaf.

No. 5: Appearance of blue-black colour in leaf shows the presence of starch in it.</br>

<strong>(28)</strong> The leaves of plant can synthesis food because they contain a green pigment chlorophyll.</br>

<strong>(29)</strong> In addition to carbon-di-oxide and water the other condition necessary for the process of photosynthesis to take place are chlorophyll and sunlight.</br>

<strong>(30)</strong> (a) an autotroph:- Grass.</br>

(b) a saprophyte:- Mushroom.</br>

(c) symbiotic plant:- Lichen.</br>

(d) a partial heterotrophy:- Pitcher plant.</br>

(e) a parasite:- Cascuta plant.</br>

<strong>(31)</strong> Organism needs to take food for getting energy and work.

The two main mode of nutrition in organisms are:- carbohydrate, vitamins.</br>

<strong>(32)</strong> Those organisms which can make food themselves from simple substances by the process of photosynthesis are called autotrophs.

Example: Green plant.</br>

<strong>(33)</strong> Those organism which cannot make food themselves from simple substances by the process of photosynthesis are called heterotrophs.

Example: Fungi.</br>

<strong>(34)</strong> We can not make food ourselves by photosynthesis like the plants do because we dont have chlorophyll.</br>

<strong>(35)</strong> Those green plants which obtain their food partly from insects are called insectivorous plant.

Example: Pitcher plant.</br>

<strong>(36)</strong> Farmers spread fartilisers and manures in the field because the soil gets enriched with nutrients like nitrogen, phosphorous and potassium.</br>

<strong>(37)</strong> A plant or animal which lives on or inside another organism called host and derives the food from it is called a parasite.

Example: Cascuta.</br>

<strong>(38)</strong> Those non green plants which obtain their food or nutrition from dead and decaying organic matter are called saprophytes.

Example: fungi.</br>

<strong>(39)</strong> Nitrogen element is present in abundance in air in the form of nitrogen gas. However the plants can not absorb nitrogen gas for their needs. Now the soil has certain bacteria which convert nitrogen gas of air into nitrogen compounds and release them into soil.</br>

<strong>(40) </strong>(a)- Nucleus.</br>

(b)- Cytoplasm.</br>

(c)- Cell membrane.</br>

(d)- Cell wall, Cytoplasm; Chloroplast.</br></br>

<strong>Long Answer Type Questions Solution:</strong></br>
</br>
<strong>(41) </strong>

Chlorophyll, the green pigment found in plants, is an integral part of photosynthesis.

<span style="color: #000000;"><strong>The role of chlorophyll:</strong></span>

Chlorophyll absorbs light energy from the sun and supplies this energy to the leaves to enable them to carry out photosynthesis for making food.</br>

(42) (a) The carbon di oxide gas present in air enters the leaves of a plant through the stomatal pores present on their surface and utilized in photosynthesis. The oxygen gas produced in the leaves during photosynthesis goes out into air through same stomatal pores. The stomatal pores open only when carbon di oxide is to be taken in or oxygen is to released otherwise they remain closed.</br>

(b) Water and minerals present in the soil are absorbed by the roots of a plant and transported to its leaves through the interconnected pipe like xylem vessels present throughout the roots, stem, branches and leaves of the plant.</br>

(43) Nutrients are replenished in the soil by adding fertilizers and manures. Fertilisers and manures contain plant nutrient such as nitrogen, phosphorus and potassium etc. so when fertilizers and manures are added to the soil in the fields then the soil gets enriched in nutrients like nitrogen, phosphorus , potassium etc.

The growing of leguminous crop in the fields beneficial to the farmer because nitrogen gas of air convert into nitrogen compounds such as nitrates. The farmer do not put nitrogen fertilizer in the fields in which leguminous crops have been grown earlier. This saves a lot of money.</br>

<strong>(44)</strong> Yes, these leaves can carryout photosynthesis. The leaves having colour red, violet, brown also have chlorophyll in them. Actually the large amount of red, violet, brown or other pigments in such leaves masks the green colour of chlorophyll. So photosynthesis also take place in leaves having colour than green.

</br></br><strong>Importance of Photosynthesis:</strong></br>

The importance of photosynthesis for existence of life on earth are given below:-

(i) Photosynthesis by plants provides food to animals including human beings.</br>

(ii) The process of photosynthesis by plants puts oxygen gas into the air.</br>

<strong>(45) (a) There are two types of modes of nutrition in plants. They are:</strong></br>

(i) Autotrophic</br>

(ii) Heterotrophic</br>

The example of Autotrophic: Green Plants.

The example of Heterotrophic: Non green plants like Fungi.</br>

<strong>(b)</strong> The living together of two different species of plants as if they are parts of the same plant and help each other in obtaining food is called symbiosis.

Example: Lichens.</div>`;
export default htmlContent;
