const htmlContent =`<h1><span style="color: #000080;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 6th Chapter <span style="color: #ff0000;">Physical And Chemical Changes</span> Solution</strong></span></h1>
<h3><span style="color: #800080;"><strong>Very Short Answer Type Questions:</strong></span></h3>
<div>(1) Zinc is used for galvanisation of iron.

</br>(2) Carbon, Chromium, nickel.

</br>(3) Crystallisation.

</br>(4) (a) Evaporation.

</br>(b) Crystallisation.

</br>(5) Pure Copper sulphate crystals.

</br>(6) Carbon Di Oxide.

</br>(7) (a) False (b) False (c) True (d) False (e) True.

</br>(8)
<table>
<tbody>
<tr>
<td width="319">Physical Changes</td>
<td width="319">Chemical Changes</td>
</tr>
<tr>
<td width="319">Dissolving sugar in water, Melting of wax, Beating aluminium to make aluminium foil</td>
<td width="319">Photosynthesis, Burning of coal, Digestion of food,</td>
</tr>
</tbody>
</table>
</br>(9) (i) A glass bottle breaking – Physical changes.

</br>(ii) Making a cake- Chemical Changes.

</br>(iii) Wool being knitted into a sweater- Physical Changes.

</br>(iv) Burning of incense stick- Chemical changes.

</br>(v) Tearing of paper- Physical changes.

</br>(vi) Cooking of food- Chemical changes.

</br>(vii) Formation of clouds- Physical change.

</br>(viii) Drying of clothes- Physical changes.

</br>(ix) Burning of paper- Chemical changes.

</br>(x) Formation of rust- Chemical change.

</br>(10) (a) chemical

</br>(b) physical; chemical

</br>(c) chemical

</br>(d) sodium hydrogencarbonate

</br>(e) Calsium carbonate

</br>(f) painting; galvanisation

</br>(g) galvanisation

</br>(h) salt

</br>(i) crystallization.
<h3><span style="color: #800080;"><strong>Short</strong> <strong>answer Type Questions:</strong>      </span></h3>
</br>(11) Iron grill painted frequently to protect them from rusting. When a coat of paint is applied to the surface of iron object then air and water can not come in contact with these iron object, hence no rusting take place.

</br>(12) This is because for protect it from rusting as zinc metal remains unaffected by air and moisture.

</br>(13) This is because to prevent it from rusting as some grease or oil is applied to the surface of an iron  object then air and moisture cannot come in contact with it thus rusting prevented.

</br>(14) Painting of an  iron gate prevent it from rusting as when a coat of paint is applied to the surface of iron object then air and water can not come in contact with these iron object, hence no rusting take place.

</br>(15) This is because the air of those palaces contains more water vapour.

</br>(16) Depositing a thin layer of zinc metal on iron objects is called galvanisation.

It is done by dipping iron sheets in molten zinc and then allowing them to cool.

</br>(17) The two method of preventing rusting are following:-

</br>(a) Rusting of iron can be prevented by painting.

</br>(b) Rusting of iron can be prevented by applying grease or oil.

</br>(18) Stainless steel is an alloy of iron.

When iron is mixed with carbon, chromium and nickel then stainless steel is made.

The important property of stainless steel is it does not rust at all.

</br>(19) The difference between physical change and chemical change are following:-

</br>(a) No new substance is formed in physical change whereas a new substance is formed in chemical change.

</br>(b) Physical change is temporary change whereas chemical change is permanent6 change.

</br>(c) Physical change is also called reversible change whereas Chemical change is irreversible change.

</br>(20) Iron (Fe) + Oxygen  (O<sub>2</sub>) + Water (H<sub>2</sub>O) à Iron oxide (Fe<sub>2</sub>O<sub>3</sub>)

</br>(21) Explosion of firework such as cracker said to be a chemical change as after bursting it can not be reversed.

</br>(22) Melting of ice form water is said to be a physical change because it can be reversible change.

</br>(23) The process of cooling a hot, concentrated solution of a substance to obtain crystals called crystallization.

<u>Use of Crystallisation:- </u>Impure copper sulphate powder can be purified by the process of crystallization to obtain large crystals of pure copper sulphate.

</br>(24) Crystals of copper sulphate are prepared by the method of crystallization.

</br>(25) (a)Burning of magnesium ribbon in air to form magnesium oxide.

</br>(b) Melting of ice to form water.
<h3><span style="color: #800080;">Long Answer Type Questions:</span></h3>
</br>(26) Physical Change:- Those changes in which no new substances are formed are called physical change.

Example: Melting of wax, Breaking of a glass tumbler.

Chemical change:- Those changes in which new substances are formed are called chemical changes.

Example:- Souring of milk, Cooking of food.

</br>(27) When an  iron  object is left damp air or water for a long time it gets covered with a red brown flaky substances called rust. This is called rusting of iron.

Condition necessary for rusting of iron:- (a) presence of oxygen (b) presence of water or water vapour.

<u>Rusting Damages Iron objects:-</u> Rusting of iron is slowly eats up the iron objects and makes them useless.

</br>(28) When iron nail is kept immersed in the copper sulphate solution  then chemical changes take places to form green coloured iron sulphate solution and a brown deposit of copper on the iron object.

Copper Sulphate Solution (Blue) + Iron (Grey) --&gt; Iron sulphate solution (Green) + Copper (Brown).

Here the chemical changes are occurred.

</br>(29) When baking soda and vinegar are mixed together, then bubbles of carbon di oxide gas are formed along with some other substances also.

Sodium hydrogencarbonate (Baking Soda) + Acetic acid (Vinegar) à Sodium Acetate + Carbon di oxide + water.

Here the chemical changes are involved.

</br>(30) Then calcium hydro oxide combines with carbon di oxide to form a white solid substances calcium carbonate.

Calcium Hydroxide Ca(OH<sub>2</sub>) + Carbon Di Oxide (CO<sub>2</sub>) à Calcium carbonate (CaCO<sub>3</sub>) + Water (H<sub>2</sub>O)

Here the chemical changes are take place.

</br>(31) It’s a chemical changes because it forms carbon di oxide as one of the new substances. As the definition of chemical change new substances are formed.

</br>(32) When a candle burns, both physical &amp; chemical changes occur. Physical Changes in Burning Candle: On heating, candle wax melts and form liquid wax. It is a physical change.

This wax vapor near to flame burns and gives new substances like Carbon Dioxide, Carbon soot, water vapours, heat and light.

</br>(33) As we know, A change which is reversible and the properties do not change of the matter.

A change which is irreversible and the properties of the matter change and form new substance.

Burning of wood is considered as a chemical change because there are new substances are formed. And which are also not reversible.

But Cutting of wood is considered as a physical change because there are no new substances are formed only their sije are changed.

</br>(34) When magnesium ribbon is burned in air then magnesium oxide is formed.

Magnesium (Mg) + Oxygen (O<sub>2</sub>) à Magnesium oxide (MgO).

This is a chemical change.

</br>(35) When magnesium oxide is dissolved in water then Magnesium hydroxide is formed.

Magnesium Oxide (Mgo) + Water (H<sub>2</sub>O) à Magnesium Hydroxide Mg(OH)<sub>2</sub> </div>`;
export default htmlContent;