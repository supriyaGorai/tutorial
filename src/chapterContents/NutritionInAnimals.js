const htmlContent = `
<div>
Lakhmir Singh Manjit Kaur Solution <strong>Class 7</strong> Chapter 2 (<strong>Nutrition in Animals</strong>) A - Z Exercise Solution Here.
<h3><span style="color: #ff0000;"><strong>Very Short Answer Type Questions Solution:</strong></span></h3>

<span style="color: #ff0000;"><strong>(1)</strong> </span>Ingestion </br>

<span style="color: #ff0000;"><strong>(2)</strong> </span>saliva</br>

<span style="color: #ff0000;"><strong>(3)</strong> </span>Peristalsis</br>

<span style="color: #ff0000;"><strong>(4)</strong> </span>(a)- Large Intestine; (b)- Small Intestine.</br>

<span style="color: #ff0000;"><strong>(5)</strong></span> Oesophagus</br>

<span style="color: #ff0000;"><strong>(6)</strong> </span>Acid</br>

<span style="color: #ff0000;"><strong>(7)</strong></span> (a) Premolars</br>

(b) Canines</br>

(c) Incisors</br>

<span style="color: #ff0000;"><strong>(8)</strong></span> (a) Milk Teeth</br>

(b) Permanent teeth.</br>

<span style="color: #ff0000;"><strong>(9)</strong></span> Milk Teeth are replace by permanent teeth.</br>

<span style="color: #ff0000;"><strong>(10)</strong> </span>At the age of 6 - 8 years milk teeth are replaced by permanent teeth.</br>

<span style="color: #ff0000;"><strong>(11)</strong></span> Tongue.</br>

<span style="color: #ff0000;"><strong>(12)</strong> </span>Salty and Sweet.</br>

<span style="color: #ff0000;"><strong>(13)</strong></span> (a) Sour Taste.</br>

(b) Bitter taste.

<span style="color: #ff0000;"><strong>(14)</strong></span> Cow.</br>

<span style="color: #ff0000;"><strong>(15)</strong> </span>Cellulose, digesting bacteria.</br>

<span style="color: #ff0000;"><strong>(16)</strong></span> Amoeba is a single celled organism has pseudopodia.</br>

<span style="color: #ff0000;"><strong>(17)</strong></span> Amoeba is single celled organism which constantly change its shape.</br>

<span style="color: #ff0000;"><strong>(18)</strong> </span>The false feet of amoeba known as pseudopodia.</br>

<span style="color: #ff0000;"><strong>(19)</strong> </span>Pseudopodia helps amoeba to keep it moving and also in capturing the food.</br>

<span style="color: #ff0000;"><strong>(20)</strong> </span>Diarrhoea.</br>

<span style="color: #ff0000;"><strong>(21)</strong></span> Diarrhoea.</br>

<span style="color: #ff0000;"><strong>(22)</strong> </span>Oral Rehydration Solution.</br>

<span style="color: #ff0000;"><strong>(23)</strong> </span>Oxygen is produced.</br>

<span style="color: #0000ff;"><strong><span style="color: #ff0000;">(24)</span> State Whether the following statement are true or false:</strong></span></br>

(a) True</br>

(b) False</br>

(c) True</br>

(d) True</br>

<span style="color: #0000ff;"><strong><span style="color: #ff0000;">(25)</span> Fill in the following blanks with suitable words:</strong></span></br>

(a) liver</br>

(b) digestive</br>

(c) vili</br>

(d) glucose; amino acids; fatty acids; glycerol.</br>

(e) digestive</br>

(f) milk</br>

(g) acid.</br>

(h) cud.</br>

(i) cow.</br>

(j) pseudopodia.</br>

(k) food vaccoule.</br>
<h3><span style="color: #ff0000;"><strong>Short Answer Type Questions Solution:</strong></span></h3>
<span style="color: #ff0000;"><strong>(26)</strong> </span>(a) Mucus protects the lining of stomach.

(b) Hydrochloric acid kills any bacteria which may enter with food.</br>

<span style="color: #ff0000;"><strong>(27)</strong> </span>A long tube running from mouth to anus of a human being in which digestion and absorption of food takes place is called alimentary canel.

The various parts of alimentary canel mouth, oesophagus, stomach, small intestine, large glands, liver, pancreas.</br>

<span style="color: #0000ff;"><strong><span style="color: #ff0000;">(28)</span> Match the items of column I and with those given in column II</strong></span>
<table>
<tbody>
<tr>
<td width="319"><span style="color: #008000;"><strong>Food Components</strong></span></td>
<td width="319"><span style="color: #008000;"><strong>Product of digestion</strong></span></td>
</tr>
<tr>
<td width="319">(i) Carbohydrates</td>
<td width="319">(b) Sugar (glucose)</td>
</tr>
<tr>
<td width="319">(ii) Proteins</td>
<td width="319">(c) amino acids</td>
</tr>
<tr>
<td width="319">(iii) Fats</td>
<td width="319">(a) Fatty acids</td>
</tr>
</tbody>
</table></br>
<span style="color: #ff0000;"><strong>(29) </strong></span>Saliva is a digestive juice which helps to digest the starch present in the food partially.</br>

<span style="color: #ff0000;"><strong>(30)</strong></span> Alimentary canel is involved in -

(a) absorption of food.

(b) Chewing of food.

(c) complete digestion of food.</br>

<span style="color: #ff0000;"><strong>(31)</strong></span> Liver secretes a liquid called bile.

Bile is stored temporarily in the sac called gall bladder.

The bile plays an important part in the digestion of fats. Actually bile converts fats into tiny droplets so that their further breakdown becomes easy.</br>

<span style="color: #ff0000;"><strong>(32)</strong></span> The food of butterfly is nectar.

Butterfly get this food by uses its long feeding tube to suck nectar from flowers.</br>

<span style="color: #ff0000;"><strong>(33)</strong> </span>The modes of taking food into the body:-

Frog - Long and cheft tongue.

Snakes- shallow the animal 'whole' which they pray upon.

Mosquitoes- long tube.

Lice - By sucking blood from the skin.

Housefly - by sucking.

Ant - biting and chewing.

Snail - by scraping it from rocks.</br>

<span style="color: #0000ff;"><strong><span style="color: #ff0000;">(34)</span> Match the items with column I with suitable items in column II:</strong></span>

(i) salivary glands - (c) saliva secretion</br>

(ii) Stomach - (d) acid release</br>

(iii) Liver - (a) Bile juice secretions</br>

(iv) Rectum - (b) storage of undigested food</br>

(v) Small Intestine - (e) digestion is complete</br>

(vi) Large intestine - (f) absorption of water</br>

(vii) Anus – (g) release of faeces.</br>

<span style="color: #ff0000;"><strong>(35)</strong> </span>The inner surface of tiny intestine has millions of tiny, finger like outgrows called villi.

Villi are located at small intestine.

The function of villi in the small intestine is to increase the surface area of the rapid apsorption of digested food.</br>

<span style="color: #ff0000;"><strong>(36)</strong></span> Bile is produced at liver.

Bile digest the food of fat.</br>

<span style="color: #ff0000;"><strong>(37)</strong> </span>This is because Glucose can be easily absorbed in blood., it provides the energy to the body. Hence, when glucose is directly taken, it does not have to be digested and thus acts as an instant source of energy.</br>

<span style="color: #ff0000;"><strong>(38)</strong> </span>Amoeba is a microscopic organism which consists of a single cell.</br>

Amoeba is found at pond water.

A similarity between the nutrition in Amoeba and human beings is that in both the cases digestive juices breakdown the complex food particles into simpler substances.

One difference between nutrition in Amoeba and Human beings is that amoeba has no mouth and no digestive system whereas a human being has mouth and digestive system.</br>

<span style="color: #ff0000;"><strong>(39)</strong></span> Amoeba takes in food by using pseudopodia.

All the undigested food gets accumulated or collected inside the cell body of amoeba.</br>

<strong><span style="color: #ff0000;">(40)</span> </strong>There are four types in our mouth -

(i) Incisors</br>

(ii) Canines</br>

(iii) Premolars</br>

(iv) Molars</br>

The function of Incisors:- The incisors are biting and cutting the food.</br>

The function of Canines:- The canines are for piercing and tearing the food.</br>

The function of Premolars:- The premolars are for chewing and grinding the food.</br>

The function of Molars:- Molars are chewing and grinding the food.</br>

<strong><span style="color: #ff0000;">(41)</span> </strong>An adult man 32 teeth.</br>

There are four incisors in one jaw.

Canines:- 2 in each jaw.</br>

Premolars:- Four in each jaw.</br>

Molars:- 6 in each jaw.</br>

<span style="color: #ff0000;"><strong>(42) </strong></span>It is easy to distinguish between the ice and ice-cream because they have distinct textures. Although cold, ice is a little hard and when broken gives you a brittle texture. Ice cream on the other hand has a creamy texture. It is also distinguish by taste, as ice-cream has particular flavour taste whereas ice  is plain water.</br>

<span style="color: #ff0000;"><strong>(43) </strong></span>The various function of tongue are following:-</br>

(a) The tongue helps in mixing saliva with food during chewing which is necessary as digestion of food.</br>

(b) The tongue helps in swallowing the food into the food pipe.</br>

(c) It is essential for talking.</br>

The four different taste which can be detected by our tongue are bitter, sour, sweet, salty.</br>

<span style="color: #ff0000;"><strong>(44)</strong></span> No, we can't survive only on grass and raw leafy vegetable. This is because The cellulose carbohydrate present in grass can be digested by the action of certain bacteria which are present only in stomach of animals called ruminants like cattle.</br>

<span style="color: #ff0000;"><strong>(45)</strong></span> (a) Bitter - Back of the tongue.</br>

(b) Sweet - Front of the tongue.</br>

(c) Sour - Sides of the tongue.</br>

(d) Salty - Front of the tongue.</br>
<h3><span style="color: #ff0000;"><strong>Long Answer Type Questions Solution:</strong></span></h3>
<span style="color: #ff0000;"><strong>(46)</strong> </span>Digestion:- The process in which the food containing large, insoluble substance is broken down into small, water soluble substances which can be absorbed by our body is called digestion.

The various organs of human digestive system are:- Mouth, Oesophagus, Stomach, Small Intestine, Large Intestine, rectum and Anus.

The name of associated glands are salivary glands, Liver and Pancreas.

<span style="color: #ff0000;"><strong>(47)</strong> </span>Tooth Decay:- Tooth decay is a process in which the tooth becomes rotten due to the formation of cavities (holes) inside it leading to toothache.

The various food are the major cause of tooth decay are:- sweets, chocolates, toffees, ice cream, cold drinks etc.

<strong>Preventing ways of tooth decay are following:-</strong>

(a) We should rinse the mouth thoroughly with clean water after every meal.

(b) We should clean our teeth with a brush and toothpaste at least twice a day.

(c) We should eat less of sugary foods such as sweets, chocolates, toffees, ice cream.

<strong><span style="color: #ff0000;">(48)</span> <span style="color: #ff0000;">Describe with the help of lebelled diagram, how feeding and digestion in amoeba takes place.</span></strong>

<img src="https://www.netexplanations.com/wp-content/uploads/2019/02/Lakhmit-Singh-Class-7-Ch-2-Q.48.jpg" alt="" /> Taking from NCERT Books of Class 7</br>

<span style="color: #ff0000;"><strong>(49)</strong> </span>(a) Ruminants:- All the animals which chew the cud are called ruminants.

Animals which are ruminants are- Cow, Sheep, Buffalo, Deer, Goat, Giraffe.

(b) Cellulose carbohydrate.

This is because The cellulose digesting bacteria are not present in the body of human beings due to which human beings cannot digest cellulose carbohydrate present in plant foods.</br>

<span style="color: #ff0000;"><strong>(50)</strong></span> The three things secreted by the inner lining of our stomach are mucus, hydrocloric acid and digestive juices.

Function of mucus:- It protects the lining of stomach.

Function of HCL:- It Kills the bacteria which may enter with food as well as it also makes the medium in the stomach acidic.

Function of digestive juice:- linining break down the proteins present in our food into simpler substances.

<strong><u>Function of Large Intestine:-</u></strong>

The large intestine absorbs the most of the water from the undigested food material.</br>

<span style="color: #ff0000;"><strong>(51)</strong></span> Incisors are the chisel shaped teeth at the front of the mouth.

The function of Incisors:- The incisors are biting and cutting the food.

Canines are the large pointed teeth just behind the incisors.

The function of Canines:- The canines are for piercing and tearing the food.

Premolars are the large teeth just behind the canines of each side. Premolars have large flat surface.

The function of Premolars:- The premolars are for chewing and grinding the food.

Molars are very large teeth which are present just behind the premolars, towards the back of our mouth. The molars have a larger surface area than premolars.

The function of Molars:- Molars are chewing and grinding the food.</br>

<span style="color: #ff0000;"><strong>(52)</strong> </span>(a) It is a bitter taste of Neem leaves or karela. Back of the tongue can detect this taste.

(b) The taste of lemon juice is sour. Sides of the tongue can detect this taste.</br>

<span style="color: #ff0000;"><strong>(53)</strong> </span>(a) The process by which the cud is braught back from the stomach to the mouth of the animal and chewed again is called rumination.

Two ruminants are Cow and Goat.

(b) This is because Cow have cellulose digesting bacteria in the body whereas humans can't digest the grass for the absence of its.</br>

<span style="color: #ff0000;"><strong>(54)</strong></span> Diarrhoea:The condition in which a person passes out watery stools frequently is called diarrhoea.

How it caused:

Diarrhoea may be caused by an infection, food poisoning or indigestion.

Dehydration take place during diarrhoea leads to the loss of water and salts from the body of a person.

Dehydration can be prevented by giving the patint Oral rehydration Solution (Saline) daily.</br>

<span style="color: #ff0000;"><strong>(55)</strong> </span>Oral rehydration is the solution of sugar and salt in water.

We can make Oral rehydration Solution at home. The steps are given below.

(i) Take a glass of clean water.

(ii) Pour a teaspoonful of sugar and a pinch of salt in it.

Oral Rehydration solution is giving to the person when he/she is suffering with diarrhoea.
</div>`;

export default htmlContent;
