const htmlContent =`<h2><span style="color: #0000ff;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 10th Chapter <span style="color: #ff0000;">Respiration in Organisms</span> Solution</strong></span></h2>
Lakhmir Singh and Manjit Kaur Science solution: Respiration in Organisms Chapter 10. Here you get easy solutions of Lakhmir Singh and Manjit Kaur Science solution Chapter 10. Here we give Chapter 10 all solution of class 7. Its help you to complete your homework.
<div class="intro-text">

<strong>Board – CBSE</strong>

<strong>Text Book - SCIENCE</strong>

<strong>Class – 7</strong>

<strong>Chapter – 10</strong>

</div>
<h3 style="text-align: center;"><span style="color: #0000ff;"><strong>Biology Chapter 10 (Respiration in Organisms) Exercise Solution – Lakhmir Singh Manjit Kaur Book</strong></span></h3>
<h3><span style="color: #ff0000;"><strong>Very Short Answer Type Questions Answers:</strong></span></h3>
<div>(1) Respiration.

</br>(2) Oxygen and Carbon Di Oxide.

</br>(3) (a) True

</br>(b) False

</br>(c) True

</br>(d) False

</br>(e) True

</br>(f) True

</br>(g) False

</br>(h) False

</br>(i) True

</br>(j) False

</br>(k) False

</br>(l) False

</br>(4) Earthworm

</br>(5) Frog

</br>(6) Fish

</br>(7) Cockroach

</br>(8) Yeast

</br>(9) Yeast

</br>(10) Anaerobic

</br>(11) Lactic acid.

</br>(12) Yeast

</br>(13) (a) Breathing rate will increase.

</br>(b) Breathing rate will decrease.

</br>(14) (a) Rib cage

</br>(b) Diaphragm.

</br>(15) Exhaled air contains more oxygen.

</br>(16) Inhaled air contains less oxygen.

</br>(17) Inhaled air contains more water vapour.

</br>(18) (a) In inhaled air-

Percentage of oxygen = 21%

Percentage of Carbon Di Oxide = 0.04%

</br>(b) In exhaled air-

Percentage of oxygen = 16.4%

Percentage of Carbon Di Oxide = 4.4%

</br>(19) Lungs.

</br>(20) (a) Oxygen is taken into the body.

</br>(b) Carbon Di Oxide is removed from the body.

</br>(21) Oxygen and Carbon di oxide are exchanged in our body.

</br>(22) Blood absorbs the oxygen in the small organelles also known as <strong>Alveoli</strong> (singular name is Alveolus).

</br>(23) Haemoglobin.

</br>(24) Nose.

</br>(25) Smoking cause lung cancer.

</br>(26) Insects

</br>(27) 15 – 18/ minute is the average breathing rate of a human adult at rest.

</br>(28) Tracheae.

</br>(29) Spiracles.

</br>(30) Stomata

</br>(31) The two parts are leaves and roots.

</br>(32) Plants part can respire independently.

</br>(33) Soil.

</br>(34) Yeast is used to make wine and beer by the process of anaerobic bacteria.

</br>(35) (a) Photosynthesis

</br>(b) Oxygen

</br>(c) Oxygen

</br>(d) alcohol

</br>(e) single

</br>(f) increase

</br>(g) Diaphragm, Rib

</br>(h) Deep

</br>(i) Inhalation

</br>(j) Exhalation

</br>(k) Lungs

</br>(l) Skin

</br>(m) Gills

</br>(n) Tracheae

</br>(o) Blowholes

</br>(p) Glucoses; Energy.
<h3><span style="color: #ff0000;"><strong>Short Answer Type Questions Answers:</strong></span></h3>
</br>(36) The process of releasing energy from food is called is called respiration.

</br><u>Word Equation of Respiration:</u>

Glucose (food) + Oxygen (from air) ----à Carbon Di Oxide + Water + Energy

</br><u>Difference between Aerobic and Anaerobic Respiration:-</u>

When the breakdown of glucose food occur with the use of oxygen is called aerobic respiration whereas when it happened without the use of oxygen is called anaerobic respiration.

</br>(37)
<table>
<tbody>
<tr>
<td width="319"><strong>Column I</strong></td>
<td width="319"><strong>Column II</strong></td>
</tr>
<tr>
<td width="319">(i) Yeast</td>
<td width="319">(c) Alcohol</td>
</tr>
<tr>
<td width="319">(ii) Diaphragm</td>
<td width="319">(d) Chest cavity</td>
</tr>
<tr>
<td width="319">(iii) Skin</td>
<td width="319">(a) Earthworm</td>
</tr>
<tr>
<td width="319">(iv) Leaves</td>
<td width="319">(e) Stomata</td>
</tr>
<tr>
<td width="319">(v) Fish</td>
<td width="319">(b) Gills</td>
</tr>
<tr>
<td width="319">(vi)  Frog</td>
<td width="319">(f) Lungs and Skin</td>
</tr>
<tr>
<td width="319">(vii) Insects</td>
<td width="319">(g) Tracheae</td>
</tr>
</tbody>
</table>
</br>(39) In aerobic respiration more energy is release because complete breakdown of glucose (food) occurs during aerobic respiration.

</br>(40) Similarities between aerobic and anaerobic respiration are –

</br>(i) Both are take place in the cells of the organism.

</br>(ii) Energy is produced by the breakdown of food.

</br>(41) Difference between aerobic and anaerobic respiration are –

</br>(i) Aerobic respiration take place in the presence of oxygen whereas anaerobic respiration take place in the absence of oxygen.

</br>(ii) Complete breakdown of food occurs in aerobic respiration whereas in aerobic respiration the breakdown of food is not totally completed.

</br>(iii) all the organisms which obtain energy by aerobic respiration cannot live without oxygen whereas in anaerobic respiration  they can live without oxygen.

</br>(42) This is because the faster breathing rate supplies more oxygen to the body cells for the speedy breakdown of food for releasing more energy which is needed for running.

</br>(43) Whenever  a person needs extra energy, he breaths faster because faster breathing rate supplies more oxygen to the body cells for the speedy breakdown of food for releasing more energy.

</br>(44) For the reason is during the exercise the leg muscles of athlete have produced extra energy by doing anaerobic respiration. By breathing faster and deeper the athlete is giving back oxygen to the muscles which it could not give earlier at the time of running that’s why he breathe faster and deeper than usual even after finishing the race.

</br>(45) We usually feel hungry after a heavy physical exercise because to provide extra energy for doing heavy physical exercise the food has broken down very rapidly and made us feel hungry.

</br>(46) If a potted plant is over watered, its roots will not get enough oxygen to breathe as water fills up the airspaces present between the soil particles. This will cause decaying of the root and atlast the plant dies.

</br>(47) exhaled air turn lime water milky more appreciably as because less carbon di oxide is present in inhaled air whereas much more carbon di oxide is present in exhaled air.

</br>(48) This is because carbon di oxide is produced during respiration which comes out in exhaled air.

</br>(49) This is because the air inhaled is used up by our body cells for respiration. Therefore exhaled air contains less oxygen than inhaled air because it has more of carbon dioxide which is one of the by products of respiration.

</br>(50) Exhaled air contains more water vapour than inhaled air because in the process of intracellular digestion with energy carbon dioxide and water vapour is also produced so the exhaled air has more water vapour than inhaled air.

</br>(51) The exhaled air which are coming from our mouth contains a lot of water vapour. This water vapor condenses on the mirror surface to form tiny droplets of water, that’s why when we exhale air on a mirror through our mouth then a patch of moisture is formed on the mirror surface.

</br>(52) All the organism respire because all living organisms need energy to continue life process and to produce energy ,generally via oxygen.

</br>(53) Yeast is single celled organism.

</br>Anaerobic respiration is carried out by yeast.

</br>(54) This is because the amount of air available to a person for breathing at high altitude is much less than that available on the ground.

</br>(55) The air around us sometimes contains various type of small unwanted particles such as dust, smoke, pollens etc. This particles get trapped in the air present in the nasal passage. Whoever some of them are entered and irritate the lining of nasal passage. As a result we often sneeze when we in hale lot of dust-laden air.

</br>(56) The harmful effects of smoking are following:-

</br>(i) Smoking is destroys the lung tissues gradually due to which breathing becomes very difficult.

</br>(ii) Smoking causes lung cancer.

</br>(iii) It causes heart disease.

</br>(57) The number of times a person breaths in one minute is called breathing rate.

Breathing rate in children is higher than that of adults. Children breathe about 20 – 30 times per minute.

</br>(58) (a) Fish – Gills

</br>(b) Earthworm- Skin

</br>(c) Frog- Skin and Lungs

</br>(d) Insects- Tracheae.

</br>(59) (a) Earthworm:- The earthworm breathes through its skin.

</br>(b) Frog:- Frog is an animal which can breathe through lungs as well as its moist skin.

</br>(c) Fish:- The fish has special organs of breathing called gills.

</br>(d) Insects:- The breathing in all the insects takes place through tiny holes which is called spiracles and thin air tubes called tracheae.

</br>(60) (a) The exchange of oxygen and carbon di oxide in the leaves during respiration takes place through stomata.

</br>(b) The respiration in roots occurs by the exchange of carbon di oxide and oxygen through the root hair.
<h3><span style="color: #ff0000;"><strong>Long Answer Type Questions Answers:</strong></span></h3>
</br>(61) (a) The breakdown of glucose food occurs with the use of oxygen it is called aerobic respiration.

</br><strong>Word Equation:-</strong>

Glucose --Oxygen--à Carbon Di Oxide + Water + Energy

Living organism normally uses aerobic respiration.

</br>(b) Anaerobic respiration take place in human muscles during heavy physical exercises.

</br>(62) (a) The breakdown of glucose food occurs without the use of oxygen it is called anaerobic respiration.

</br><strong>Word Equation:-</strong>

Glucose – No Oxygen--à Alcohol + Carbon Di Oxide + Energy

The microscopic organism like yeast always use anaerobic respiration.

</br>(b) Aerobic respiration usually take place in humans.

</br>(63) (a) The main organs of human respiratory system are:-

Nose, Nasal passages, Trachea, Bronchi, Lungs and Diaphragm.

</br>(64) Breathing is these by which air rich in oxygen is taken inside the body of an organism and air rich in carbon di oxide is expelled from the body.

<strong>Mechanism of breathing in humans:</strong>

<img  src="https://www.netexplanations.com/wp-content/uploads/2019/03/Mechanism-of-breathing-in-humans.jpg" /> Mechanism of Breathing in Humans

</br><strong>(65) (a) </strong>Yawning is caused by insufficient<strong>  </strong>oxygen  in the body.

We usually Yawning when we tired, bored, stressed, over worked, feel sleepy drowsy etc.

</br>(b) The breathing rate are slow down when we tired, bored, stressed, over worked, feel sleepy drowsy.

Yawning helps in bringing more oxygen into our blood because at the time of yawning our mouth opens more and we take long and deep breath of air.</div>`;
export default htmlContent;