const htmlContent =`
<h1><strong><span style="color: #000080;">Lakhmir Sing Manjit Kaur Class 7 Science Book Chapter 3 <span style="color: #ff0000;">FIBRE TO FABRIC</span> Exercise Solution With Easy Answers</span></strong></h1>
<h2 style="text-align: center;"><span style="text-decoration: underline; color: #00ff00;"><strong><span>FIBRE TO FABRIC</span></strong></span></h2>
<h3><span style="color: #000000;"><strong>Very Short Type Questions:</strong></span></h3>
<div><strong><span style="color: #0000ff;">(1) Two fibres obtained from animals are wool and silk.</span></strong></br>

<strong><span style="color: #0000ff;">(2) Yak wool is common in the hilly regions of Tibet and Ladakh.</span></strong></br>

<strong><span style="color: #0000ff;">(3) Angora goats found in Jammu &amp; Kashmir.</span></strong></br>

<strong><span style="color: #0000ff;">(4) Kashmiri goat.</span></strong></br>

<strong><span style="color: #0000ff;">(5) Llama and Alpaca are found at South America.</span></strong></br>

<strong><span style="color: #0000ff;">(6) The two breeds of sheep found in India are Lohi and Rampur bushair.</span></strong></br>

<strong><span style="color: #0000ff;">(7) Sheep are herbivores.</span></strong></br>

<strong><span style="color: #0000ff;">(8) (a) Shearing.</span></strong></br>

<strong><span style="color: #0000ff;">(b) Scouring</span></strong></br>

<strong><span style="color: #0000ff;">(c) Sorting.</span></strong></br>

<strong><span style="color: #0000ff;">(9) Bacteria called anthrax</span></strong></br>

<strong><span style="color: #0000ff;">(10) Shearing.</span></strong></br>

<strong><span style="color: #0000ff;">(11) Silk</span></strong></br>

<strong><span style="color: #0000ff;">(12) The adult silk moth spins cocoons from which silk is 0obtained. (False)</span></strong></br>

<strong><span style="color: #0000ff;">(13) Mulberry trees.</span></strong></br>

<strong><span style="color: #0000ff;">(14) The natural colours of the fleece of sheep and goats are white, brown or brown.</span></strong></br>

<strong><span style="color: #0000ff;">(15) Larva</span></strong></br>

<strong><span style="color: #0000ff;">(16) Larva.</span></strong></br>

<strong><span style="color: #0000ff;">(17) Sericulture</span></strong></br>

<strong><span style="color: #0000ff;">(18) The two fibres which are made of proteins are Wool, fur.</span></strong></br>

<strong><span style="color: #0000ff;">(19) Plant Fibre.</span></strong></br>

<strong><span style="color: #0000ff;">(20) Fill in the following blanks with suitable words:</span></strong></br>

<strong><span style="color: #0000ff;">(a) breeding</span></strong></br>

<strong><span style="color: #0000ff;">(b) goat</span></strong></br>

<strong><span style="color: #0000ff;">(c) wool</span></strong></br>

<strong><span style="color: #0000ff;">(d) herbivores</span></strong></br>

<strong><span style="color: #0000ff;">(e) sheep</span></strong></br>

<strong><span style="color: #0000ff;">(f) cocoon</span></strong></br>

<strong><span style="color: #0000ff;">(g) protein</span></strong></br>
</br>
<strong><span style="color: #0000ff;">(h) larvae (or caterpillars).</span></strong></br>

<strong><span style="color: #0000ff;">(i) cocoon</span></strong></br>

<strong><span style="color: #0000ff;">(j) reeling</span></strong></br>
<h3><strong>Short Answer Type  Questions</strong></h3>
<strong><span style="color: #800080;">(21) Mostly sheep eat grass. Except of grass sheep are also fed mixture of pulses, corn, jowar, oil cakes and minerals.</span></strong></br>

<strong><span style="color: #800080;">(22) Rearing of sheep means to look after the sheep by providing them food, shelter and health care.</span></strong></br>

<strong><span style="color: #800080;">Breeding of sheep is done to obtain such breeds of sheep which yield good quality wool in large quantities.</span></strong>

<strong><span style="color: #800080;">(23) The various steps involved in the production of wool from sheep are Shearing, Scouring, Sorting, Dyeing, Combing, Spinning.</span></strong></br>

<strong><span style="color: #800080;">(24) Wool yielding animals like sheep have a thick coat of hair on their body because to keep them warm during cold winter season.</span></strong></br>

<strong><span style="color: #800080;">(25) Woollen clothes keep us warm during winter because wool is a poor conductor of heat and it has air trapped in between the fibres.</span></strong></br>

<strong><span style="color: #800080;">(26) The risks faced by people working in any industry due to the nature of their work are called occupational hazards.</span></strong></br>

<strong><span style="color: #800080;">Sorter's disease is an occupational hazard.</span></strong>

<strong><span style="color: #800080;">(27) The rearing of silkworms for obtaining silk is called sericulture.</span></strong></br>

<strong><span style="color: #800080;">(28) The most common silkmoth is mulberry silkmoth.</span></strong></br>

<strong><span style="color: #800080;">Silk fibres obtained from the cocoons are spum to form silk threads called silk yarn. The silk yarn is then woven on looms into silk cloth by the weavers.</span></strong>

<strong><span style="color: #800080;">(29) We can distinguish between natural silk and artificial silk by performing burning test.</span></strong></br>

<strong><span style="color: #800080;">(30) The process of taking out silk fibers from the coocons for use as silk is called reeling.</span></strong></br>

<strong><span style="color: #800080;">Reeling is done in special machines which unwind the fibers of silk from coocons.</span></strong>
<h4><strong>(31) Match the words of column I with those given in column II</strong></h4>
<strong><span style="color: #333399;">(i) Scouring- (e) Cleaning Sheared wool.</span></strong></br>

<strong><span style="color: #333399;">(ii) Mulberry leaves- (c) Food of silkworms.</span></strong></br>

<strong><span style="color: #333399;">(iii) Yak- (b) wool yielding animal.</span></strong></br>

<strong><span style="color: #333399;">(iv) Coocon- (a) Yields silk fibres.</span></strong></br>
<h4><strong>(32) Five animals which yield wool:- sheep, goat, yak, camel, llama and alpaca.</strong></h4>
<strong><span style="color: #333399;">Wool is most commonly obtained from sheep.</span></strong>
<h4><strong>(33) The four types of silk are:- Mulberry silk, Tassar silk, Mooga silk, Kosa silk.</strong></h4>
<strong><span style="color: #333399;">silk is an animal fibre.</span></strong>
<h4><strong>(34) The silk covering spun by the silkworm of silk moth is called cocoon.</strong></h4>
<span style="color: #333399;"><strong>The cocoon is made by silkworm to protect its development as Pupa.</strong></span>
<h4>(35) Shearing &gt; Scouring &gt; Sorting &gt; Dyeing &gt; Combing &gt; Spinning.</h4>
<h3><strong>Long Answer Type Questions:</strong></h3>
(36)

<img src="https://www.netexplanations.com/wp-content/uploads/2019/02/Taking-from-NCERT.png"  />

<span style="color: #000080;"><strong><span style="color: #000000;">(37) (a)</span> Shearing:- The process of removing hair from the body of a sheep in the form of fleece is called Shearing.</strong></span></br>

<span style="color: #000080;"><strong><span style="color: #000000;">(b)</span> Scouring:- The process of washing the fleece thqat removes dust, dirt, dried sweat and grease is called scouring.</strong></span></br>

<span style="color: #000080;"><strong><span style="color: #000000;">(c)</span> Sorting:- The process of separating the fleece of a sheep into sections according to the quality of woolen fibres is called sorting.</strong></span></br>

<span style="color: #000080;"><strong><span style="color: #000000;">(38)</span> Egg -&gt; Larva (or Caterpillar) -&gt; Pupa -&gt; Silk Moth.</strong></span>

<span style="color: #000080;"><strong>Follow the page No. 36 (Life History of Silk Moth).</strong></span></br>

<span style="color: #000080;"><strong><span style="color: #000000;">(39) (a)</span> Rearing of Silkworms to obtain Coocons:</strong></span></br>

<span style="color: #000080;"><strong>A female silk moth lays hundreds of eggs at a time. The eggs of silk moth are stored carefully and sold to the farmers. The farmers keep this egg at a suitable teperature and humidity so that these eggs can hatch.</strong></span>

<span style="color: #000080;"><strong>After hatching silkworms comes out. Silkworms now fed up cut mulberry leaved. After 1 month silkworms stop eating and ready to spin coocon. The silkworm now climb up the twigs place near them and spin coocons of silk fibres.</strong></span>

<span style="color: #000080;"><strong><span style="color: #000000;">(b)</span> Processing of coocons to obtain silk fibres:</strong></span>

<span style="color: #000080;"><strong>All the coocons are collected at one palce. The pile of coocon is placed in hot water. Hot water separate out silk fibres of coocons.</strong></span></br>

<span style="color: #000080;"><strong><span style="color: #000000;">(c</span>) Converting silk fibres into silk cloth:-</strong></span>

<span style="color: #000080;"><strong>Silk fibres are then spun into silk threads, which are woven into silk cloth by weavers.</strong></span></br>

<span style="color: #000080;"><strong><span style="color: #000000;">(40)</span> The shearing of sheep is dine in hot season. This is because sheep may survive without their protecting coat of hair.</strong></span>

<span style="color: #000080;"><strong>Shearing doeas not hurt the sheep because the uppermost layer of the skin is dead. Like when we cutted our hear is their any pain of face? No. It is the same.</strong></span>

&nbsp; </div>`;
export default htmlContent;