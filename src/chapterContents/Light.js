const htmlContent =`<strong>Lakhmir Singh and Manjit Kaur Science solution:</strong> Light Chapter 15. Here you get easy solutions of Lakhmir Singh and Manjit Kaur Science solution Chapter 15. Here we give Chapter 15 all solution of class 7. Its help you to complete your homework.
<ul>
 	<li><strong>Board – CBSE</strong></li>
 	<li><strong>Text Book - SCIENCE</strong></li>
 	<li><strong>Class – 7</strong></li>
 	<li><strong>Chapter – 15</strong></li>
</ul>
<h3><span style="color: #ff0000;"><strong>Very Short Answer Type Questions Answers:</strong></span></h3>
<div>(1) virtual,.

</br>(2) 10 cm.

</br>(3) (a) Virtual Image

</br>(b) Real Image.

</br>(4) Laterally Inverted image is formed.

</br>(5) Surface of water in the lake.

</br><strong>(6) </strong>Plane mirror is required.

</br>(7) lateral Inversion.

</br>(8) Rectilinear propagation of light

</br>(9) If we lift our right hand in front of big plane mirror then our image lifts its left hand and If we lift our left hand then our image lifts its right hand.

</br>(10)  lateral Inversion.

</br>(11) (a) true

</br>(b) false

</br>(c) False

</br>(d) True

</br>(d) True

</br>(12) (a) Convex mirror.

</br>(b) Concave mirror.

</br>(13) (a) Concave mirror

</br>(b) Convex mirror.

</br>(14) Its parabolic, but the radius is so big it can be approximated to a part of a sphere.

</br>(15) Concave mirror

</br>(16) Concave lens.

</br>(17) When the object is kept between Pole and Focus of the concave mirror. Virtual erect and enlarged/magnified image is produced. Example : the dentist uses the concave mirror during dental check-up .

</br>(18) Concave mirror.

</br>(19) (a) Concave lens

</br>(b) Convex lens.

</br>(20) convex lens.

</br>(21) Convex lens is the lens which can convert sun rays to a point called focus and burn a hole in a piece of paper.

</br>(22) Convex lens is a converging lens.

</br>(23) Object should be placed at a distance less than focal length order to use a convex lens as a magnifying glass.

</br>(24) Concave lens

</br>(25) Convex lens.

</br>(26) spectrum

</br>(27) The Water Droplets present in the air acts as tiny prisms in the formation of a rainbow.

</br>(28) The rainbow is produced by the dispersion of sun light by tiny raindrops suspended in the atmosphere.

</br>(29) We can demonstrate the mixture of seven colors.

</br>(30) (a) Straight

</br>(b) Reflecting

</br>(c) real

</br>(d) Virtual

</br>(e) Plane

</br>(f) Convex

</br>(g) Concave

</br>(h) Thicker; Thinner

</br>(i) diverges; converges.

</br>(j) concave, convex

</br>(k) lens

</br>(l) Smaller

</br>(m) seven

</br>(n) seven

</br>(o) white

</br>(p) white

</br>(q) seven.
<h3><span style="color: #ff0000;"><strong>Short Answer Type Questions Answers:</strong></span></h3>
</br>(31) Two type of spherical mirror is :- Concave mirror, Convex mirror.

Concave mirror can form real image.

</br>(32) The image which can be obtained on a screen are called real image. Whereas The image which cannot be obtained on a screen called virtual image.

Virtual image is formed in still water or lake.

</br>(33) A curved mirror is a mirror with a curved reflecting surface The surface may be either convex (bulging outwards) or concave (bulging inwards). Most curved mirrors have surfaces that are shaped like part of a sphere, but other shapes are sometimes used in optical devices.

Concave mirror are used as shaving mirrors to see a large image of the face.

Convex mirror are used as rear view mirrors.

</br>(34|) The difference between convex lens and concave lens are:-

</br>(a) The lens which is thicker in the middle than at the edges is called convex lens. Whereas the lens which is thinner in the middle is called concave lens.

<strong>Uses of Convex Lens:-</strong>

Convex lenses are used as magnifying glass.

<strong>Uses of Concave Lens:-</strong>

Concave lenses are used in making spectacles.

</br>(35)
<table>
<tbody>
<tr>
<td width="319">
<p style="text-align: center;"><strong>Column I</strong></p>
</td>
<td width="319">
<p style="text-align: center;"><strong>Column II</strong></p>
</td>
</tr>
<tr>
<td width="319">(i) A Plane mirror</td>
<td width="319">(e) The image is erect and smaller in size than the object</td>
</tr>
<tr>
<td width="319">(ii) A convex mirror</td>
<td width="319">(b) Can form image of objects spread over a large area; (f) The image is erect and smaller in sije than the object</td>
</tr>
<tr>
<td width="319">(iii) A convex lens</td>
<td width="319">(a) Used as a magnifying glass</td>
</tr>
<tr>
<td width="319">(iv) A concave mirror</td>
<td width="319">(c) Used by dentist to see enlarged images of teeth</td>
</tr>
<tr>
<td width="319">(v) A concave lens</td>
<td width="319">(f) The image is erect and smaller in sije than the object</td>
</tr>
</tbody>
</table>
</br>(36) The characteristics of image formed by plane mirror are:-

</br>(i) The image formed by plane mirror is behind the mirror.

</br>(ii) The image formed in a plane mirror is virtual. It can not be obtained on a screen.

</br>(iii) The image formed in a plane mirror is of the same size of the object, It is neither enlarged nor diminished.

</br>(37) Concave mirror are used as shaving mirrors to see a large image of the face.

</br>(38) When an object is placed at a far off distance from a concave mirror, the image formed by concave mirror is (a) real (b) inverted (c) Much smaller than the object.

</br>(39) When an object is placed close to concave mirror, the image formed by concave mirror is-

</br>(i) Virtual

</br>(ii) Erect

</br>(iii) Larger than the object.

</br>(40) When an object is placed close to a convex lens the image formed by convex lens is:-

</br>(i) Virtual

</br>(ii) Erect

</br>(iii) Larger than the object.

</br>(41) When an object is placed at a distance between focal length and twice the focal length of a convex lens then the image formed by convex lens is :-

</br>(i) Real

</br>(II) Inverted

</br>(iii) Larger than the object.

</br>(42) When an object is placed at a distance between focal length and twice the focal length from a convex lens the image formed-

</br>(i) Virtual

</br>(ii) Erect

</br>(iii) Smaller than the object.

</br>(43) (i) If I stand close to a large concave mirror the image look bigger than me.

</br>(ii) In Convex mirror the image look smaller than me.

</br>(44) (a) A convex mirror make: real or inverted /  Virtual or erect images.

</br>(b) A concave lens make (i) Virtual (ii) Erect and (iii) Smaller than the object.

</br>(45) (a) (i) Take a Glass prism.

</br>(ii) Allow a narrow beam of sunlight through a small hole in the window of a dark room to fall on one face of the glass prism.

</br>(iii) Let the light coming out of the other face of the prism fall on a white sheet of paper or on a white wall.

</br>(iv) A patch of seven color will be formed on the white sheet of paper.

</br>(b) Newton discovered that sunlight consist of seven colors.

</br>(46) (a) Two observation are:-

If we shine a torch on a dark rook we will see, that the beam of light produced by torch travels straight into darkness.

The beams of light from the headlight of car.

</br>(b) Newton demonstrate that, consist of mixture of lights of seven colors.

</br>(47) (a) Pencil would look very small; Virtual image.

</br>(b) Close to the convex lens.

</br>(48) (a) The seven colors is formed when a beam of sunlight is passed through a glass prism.

The band of seven colors formed on a white screen when a beam of white light is passed through a glass prism is called spectrum of white light.

</br>(49) There are seven colors in the white light.

The various colors of white light is : Red, orange, Yellow, Green, Blue, Indigo and Violet.

</br>(50) Yes we mix seven colors to get white light. Take a compact disc, afterthat paint the seven colors or put the seven color of paper on the disc. Now make a small hole from the back side. Enter a head of refill such a way that the disc can be rotate freely. Now rotate this in speedy way. We can see the mixed of seven colors formed White color.
<h3><span style="color: #ff0000;"><strong>Long Answer Type Questions Answer:</strong></span></h3>
</br>(51) (a) Lens is a piece of transparent glass bound by two spherical surfaces.

Two types of lens are Convex Lens, Concave Lens.

Five things which use lens are :- Spectacle, magnifying glass, telescope, microscope, camera, projector,etc.

</br>(b) A convex lens is used as a magnifying glass.

</br>(c) Concave mirror is used in it .When a source of light is placed at the focus of a concave mirror, a parallel beam of light rays is obtained.

</br>(52) (a) The splitting up of white light into seven colors on passing through a transparent medium like a glass prism is called dispersion of white light.

The natural phenomenon is – (a) Rainbow.

</br>(b) This is because the image formed in a plane mirror is laterally inverted with respect to the object.

</br>(53) (a) The rainbow is an arch of seven colors seen in the sky.

The rainbow is formed by the dispersion of sunlight by tiny rain drops suspended in the atmosphere.

The info we get from the formation of rainbow that, white light consist of seven colors.

</br>(b) The ambulance written on the hospital van as the way of laterally inverted.

This is because, when we are driving in our car and see the ambulance coming from the behind in our rear view mirror, them the will get the laterally inverted image of ‘Ambulance’ as AMBULANCE.

</br>(54) (a) The image which can be obtained on a screen called real image. Example:- Cinema Screen.

</br>(b) The image which cannot be obtained on a screen is called virtual image. Example:- Our Image in a plane mirror.

</br>(c) Two characteristics are:-

</br>(i) The image formed by plane mirror is behind the mirror.

</br>(ii) The image formed in a plane mirror is virtual. It can not be obtained on a screen.

</br>(d) Three characteristics of the image of an object formed by a concave lens:-

</br>(i) Virtual

</br>(ii) Erect

</br>(iii) Smaller than the object.

</br>(55) (a) The table doesn't give its light but also we are able to see it because it has seen because of reflection of another light in our eyes.

</br>(b) Three characteristics are:-

</br>(i) A convex mirror always forms a virtual image of an object.

</br>(ii) A convex mirror the image is always smaller than the object.

</br>(iii) A convex mirror always forms erect image of an object.

</br>(c) Dentists used Concave mirror. It help the dentists to see the large image of the teeth of patients.

</br>(d) There are seven colors in the rainbow. They are: Red, Green, Yellow, Blue, Violet, Indigo, Orange,</div>`;
export default htmlContent;