const htmlContent =`<h1><span style="color: #000080;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 14th Chapter <span style="color: #ff0000;">Electric Current And Its Effects</span> Solutions</strong></span></h1>
&nbsp;

Lakhmir Singh and Manjit Kaur Science solution: Electric Current And Its Effects Chapter 14. Here you get easy solutions of Lakhmir Singh and Manjit Kaur Science solution Chapter 14. Here we give Chapter 14 all solution of class 7. Its help you to complete your homework.

Board – CBSE

Text Book - SCIENCE

Class – 7

Chapter – 14
<h3><span style="color: #800080;"><strong>Very Short Answer Type Questions:</strong></span></h3>
<div>(1) Switch

</br>(2) Connecting Wires

</br>(3) (a) battery of Two Cells.

</br>(b) Electric Bulb (Electric Lamp).

</br>(4) Battery.

</br>(5) (a) False

</br>(b) False

</br>(c) True

</br>(d) False

</br>(e) False

</br>(f) False

</br>(g) False

</br>(h) False

</br>(i) True

</br>(j) True

</br>(6) Two effects produced by electric current are Heating effect and Magnetic effect.

</br>(7) (a) Heating effect

</br>(a) Heating effect

</br>(8) Nichrome

</br>(9) Tungsten

</br>(10) <strong>Heating effect of electric current is utilised in the working of an electric fuse.</strong></br>

<strong>(11) Compact Fluorescent Lamp.</strong></br>

<strong>(12) (a) Short Circuit</strong></br>

<strong>(b) Overloading</strong></br>

<strong>(13) (a) Miniature Circuit Breaker (MCB)</strong></br>

<strong>(b) Electric fuse</strong></br>

<strong>(14) MCB</strong></br>

<strong>(15) Magnetic effect of current.</strong></br>

<strong>(16) Hans Christian Oersted.</strong></br>

</br>(17) Electromagnet.

</br>(18) The piece of iron.

</br>(19) Electric Bells, Loudspeakers.

</br>(20) Electromagnet.

</br>(21) Iron

</br>(22) Magnetic effect.

</br>(23) DC motor.

</br>(24) Electromagnet

</br>(25) Heating effect of electric current is utilised in an electric bell.

</br>(26) Copper

</br>(27) Electromagnet

</br>(28) No

</br>(29) <strong>Miniature Circuit Breaker.</strong></br>

<strong>(30) (a) Electric Current</strong></br>

<strong>(b) Positive</strong></br>

<strong>(c) Negative</strong></br>

<strong>(d) battery</strong></br>

<strong>(e) Red-Hot</strong></br>

<strong>(f) Fuse</strong></br>

<strong>(g) Electromagnet</strong></br>

<strong>(h) Magnetic</strong></br>

<strong>(i) Electromagnet</strong></br>

<strong>(j) Magnet</strong></br>
<h3><span style="color: #800080;"><strong>Short Answer Type Questions Answers:</strong></span></h3>
<strong>(31) A continuous conducting path between the two terminals of a cell or battery along which an electric current flows is called a electric circuit.</strong>

<strong>Diagram is placed.</strong></br>

<strong>(32) Two electrical components are Cell and Battery.</strong></br>

<strong>(38) Five appliances in which electric cells are used :- </strong>Gyeser, Remote, Clock Wall , Remote Control Car, Torch.

</br>(39) Two applications of heating effect of electric current are following:-

</br>(i) The heating effect of electric current is utilized in the working of electrical heating appliances such as electric room heater, electric iron, Electric kettle, Geyser etc.

</br>(ii) The heating effect of electric current is utilized in electric bulbs for producing light.

</br>(40) Flow of excessive current in households circuit is the short circuit (touching of live wire and neutral wire.).

</br>(41) Electric fuse, prevents electric fires and damage to electrical appliances due to excessive flow of current.

</br>(42) When too many electrical appliances are connected to a single socket they draw an extremely large current from house hold circuit. This way overloading of a household electric circuit occur.

</br>(43) This is because it has a high melting point. Due to which it will not melt easily when a large current passes through it.;

</br>(44) Cartridge fuse. It consists of a glass tube T having a thin fuse wire sealed inside it. The glass tube has two metal caps at its two ends. The two ends of the fuse wire are connected to these metal caps. The metal caps are for connecting the fuse in the circuit in a suitably made bracket.

</br>(45) No, I am not agree. This is because we should not use a thick wire as a fuse wire because it will have a low resistance and hence it will not get heated to its melting point easily.

</br>(46) Then the large current flowing through the wiring could heat up the wires to very high temperatures and cause electric fires.

</br>(47) This is because a major part of the electricity consumed by the filament of an  electric bulb is converted into it due to which the bulb becomes hot, only a small part of electricity is converted into light.

The two type of electric lighting devices which are much more energy efficient than filament type of bulbs are  (a) Fluorescent tube light (b) Compact Fluorescent Lamp.

</br>(48) No, an electromagnet cannot be used to separate plastic from garbage heap. Magnets only attract magnetic elements which include all the elements lying in the d-block of periodic table. Since plastic is neither a d-block nor it is a magnetic alloy, hence electromagnet cannot be used to separate plastic waste from garbage heap.

</br>(49) Two important use of electromagnet:-

</br>(a) Electromagnets are used in the construction of a large number of devices like electric bells, loudspeakers, electric motors, electric fans, toys and telephone instrument etc.

</br>(b) this are used to lift heavy loads like big machines, steel griders and scrap iron objects for loading and unloading purposes.

</br>(50) This is due to high permeability factor, iron is used for making electromagnet. Also, iron creates strong field due to which it forms permanent magnet.

whereas, steel being an alloy of iron acts as a temporary magnet. So we cannot use the steel for making electromagnet.
<h3><span style="color: #800080;"><strong>Large Answer Type Questions Answers:</strong></span></h3>
</br>(51) <strong><u>Electromagnet:</u></strong> The magnet made by using electric current is called an electromagnet.

<strong><u>Activity to make an Electromagnet</u></strong></br></br>

<strong>(Page No.: 240)</strong></br></br>

<strong>(52) Two advantages of electromagnet over permanent are:</strong>

</br>(i) The magnetism of an electromagnet can be switched on or off as desired. This is not possible with a permanent magnet.

</br>(ii) An electromagnet can be made very strong by increasing the number of turns in the coil and by increasing the current passing through the coil.

Electromagnet is used in an electric bell.

</br><strong>(53) (a) When an electric current is passed through a high resistance wire the resistance wire becomes hot and produces heat. This is called heating effect of current.</strong>

<strong>Two appliances are: Greaser, Room heater.</strong></br>

</br><strong>(b) Heating effect depends on the resistance, time and current in the conductor. Heater element is made up of high resistance and connecting wire has less resistance as compared to heater element.</strong></br></br>

</br><strong>(54) A group of cells joined in series is called battery.</strong>

<strong>We combine two cells by keeping the positive terminal of one cell in contact with the negative terminal of other cell.</strong></br></br>

</br><strong>(55) (a) Five electrical appliances in which the heating effect of current is utilized: Electric Room Heater, Electric Iron, Electric Oven, Geyser, Soldering Iron.</strong></br>

</br><strong>(b) This is because when a current is switched on through a wire, the wire starts behaving as a magnet. Hence, when a compass needle is placed near the given current carrying wire, it gets influenced by the magnetic effect of electric current and gets deflected from its north- south position.</strong></div>`;
export default htmlContent;