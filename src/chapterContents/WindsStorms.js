const htmlContent =`<div>Lakhmir Singh and Manjit Kaur Science solution: Winds, Storms And Cyclones Chapter 8. Here you get easy solutions of Lakhmir Singh and Manjit Kaur Science solution Chapter 8. Here we give Chapter 8 all solution of class 7. Its help you to complete your homework.

Board – CBSE

Text Book - SCIENCE

<span style="color: #ff0000;"><strong>Class – 7</strong></span></br>

<span style="color: #0000ff;"><strong>Chapter – 8</strong></span>
<h3><span style="color: #000080;"><strong>Very Short Answer Type Questions:</strong></span></h3>
</br>(1) Air Pressure.

</br>(2) Air exert pressure.

</br>(3) Blowing wind exert a force.

</br>(4) Warm air.

</br>(5) (a) Upper region

</br>(b) Middle region

</br>(6) (a) Hurricane

</br>(b) Typhoon.

</br>(7) Reduced Air Pressure.

</br>(8) (a) Anemometer

</br>(b) Wind vane.

</br>(9) Monsoon winds.

</br>(10) South West Direction.

</br>(11) (a) False; (b) False; (c) True; (d) False; (e) True

</br>(12) Thunderstorm, Cyclone, Tornado.

</br>(13) Thunder storms.

</br>(14) (a) East coast

</br>(b) West coast

</br>(15) Cyclone

</br>(16) (a) Releases heat

</br>(b) Absorbs heat

</br>(17) Cyclones

</br>(18) Tornado

</br>(19) Centre of Cyclone called eye of cycle.

</br>(20) (a) Wind

</br>(b) Pressure

</br>(c) Expand, Contracts

</br>(d) Lighter

</br>(e) Warm; Cold

</br>(f) Uneven

</br>(g) High; Low

</br>(h) Faster

</br>(i) reduced

</br>(j) rotation

</br>(k) eye

</br>(l) Cyclone

</br>(m) tornado

</br>(n) West, East

</br>(o) Hurricane, Typhoon
<h3><span style="color: #000080;"><strong>Short Answer Type Questions:</strong></span></h3>
</br>(21) Holes are made in hanging banner so that high speed wind may pass through them easily without damaging them or bringing them down with its huge force or pressure.

</br>(22) Bicycle tube, Air filled into a balloons tell us that air exerts pr5essure.

</br>(23) The balloon gets inflated cause the air filled in it exerts pressure.

</br>(24) The air filled in bicycle tube keep it tight because when we fill air into it then the air molecules inside the tube collide with the walls of the tube and exert air pressure.

</br>(25) This is because the air pressure in the bicycle tube will increase too much due to which the bicycle tube may even burst.

</br>(26) The football gets inflated because the air filled in it exerts pressure.

</br>(27) The leaves of trees, flags and bananas flutter when the wind is blowing cause air exert pressure on them.

</br>(28) The conclusion do we get that the blowing wind exerts pressure on the boat in the direction of their motion and helps it move forward.

</br>(29) The conclusion do we get the wind coming from our back side, strikes the kite and exerts pressure on the kite to make it fly higher and higher.

</br>(30) When air is heated its volume increases  and it occupies a bigger space. We say that air expands on heating.

</br>(31) This is because hot air is lighter than cold air so rise upwards.

</br>(32) This is because smoke is produced by the burning of a substance, therefore smoke is always accompanied by hot air. As we know warm air is lighter than cold air so smoke is rises up.

</br>(33) No, Because the warm air of the room rises up and it will pass through the ventilator.

</br>(34) Wind is mixed up with humidity which is the main cause of wind movement on the surface of the earth.

</br>(35) The two situation are:-

</br>(i) Uneven heating between the equator and poles of the earth.

</br>(ii) Uneven heating of land and water of oceans.

</br>(36) Check the Book.

</br>(37) The winds produced by the uneven heating of the earth between the equator and the poles do not blow in the exact north-south direction because a change in the direction of winds is caused by the rotation of the earth on its axis.

</br>(38) Check the Book.

</br>(39) The strip of paper lifts up because when we blow air over the surface of paper strip. The air pressure below the paper strip, being higher, pushes the paper strip upwards and lifts it up.

</br>(40) This activity shows up that fast moving air is accompanied by low air pressure or reduced air pressure.

</br>(41) When we blow air between the balloons then the fast moving air creates a region of low air pressure in the space between the two balloons. The air pressure on the outside of the balloons being higher pushes the two balloons towards each other and makes them come closer.

</br>(42) This is because when we blow air into the mouth of the bottle then the air in the neck of bottle has high speed. This fast moving air reduces the air pressure in the neck of bottle. The air pressure inside the bottle being higher, constantly pushes the paper ball out and does not allow I to go inside the bottle.

</br>(43) This is because If high speed winds blow over the roofs of houses, they will reduce the air pressure above the roofs. If the roofs of the house are weak then higher air pressure from below will lift up the roofs which can then be blown away by the fast winds.

</br>(44) No. (a) is correct, that in winter the winds blow from the land to the oceans.

</br>(45) The winds blowing from the oceans to the land in summer is called monsoon winds.

Monsoon winds are important because this winds carry a lot of water from the oceans and bring large amount of rains on land.
<h3><span style="color: #000080;"><strong>Long answer Type Questions:</strong></span></h3>
</br><strong>(46) Tornado:-</strong> A tornado is a violent storm with a column of rapidly rotating winds, having the appearance of a dark, funnel shaped cloud that reaches from the sky to the ground.

<strong><u>Damage caused by Tornado:-</u></strong>

</br>(i) Tornado uproots trees, electric poles, disrupting power supply and telecommunication.

</br>(ii) Tornado lifts up the people and vehicle off the ground and hurls them hundred of meters away that’s why many people can die from this activity.

</br>(iii) Tornado can also explode the building.

</br><strong><u>Precaution from Tornado:-</u></strong>

</br>The precaution should be taken are following:-

</br>(i) Take shelter a room situated deep inside the house having no windows.

</br>(ii) If a room having no without windows then shut down all windows and doors.

</br>(iii) If a person is in vehicle and that time tornado is started then he  have to get out of the vehicle and go to the low lying area and lie flat in it.

</br>(47) Cyclone:- is a huge revolving storm which is caused by very high speed winds blowing around a central area of very low pressure in the atmosphere.

In the atmosphere over the warm sea water near the equator during the hottest summer months, cyclone is basically formed.

The five factors are- temperature, humidity, wind speed, wind direction and rotation of earth.

The centre of cyclone known as ‘eye of the cyclone’.

</br>(48) Cyclone are so destructive because this can damage building, railway tracks, damage vehicles, death of people, cause flood, pollute the water during flood, destroy crops and many more.

</br><strong>Cyclone causes widespread destruction leading to a great loss of life and property explained as follows-</strong>

</br>(i) The high speed with cyclone uproots building, trees, poles of electric line, telephone, hurl up people causing lot of damage..

</br>(ii) The fast moving of flood water with cyclone damage rail tracks, destroy crops etc.

<strong>The precaution are following:-</strong>

</br>(i) Do not drink water that could be contaminated by floods.

</br>(ii) Do not touch wet electric switches.

</br>(iii) Do not enter damage building etc.

</br>(49) Thunderstorm:- A storm with thunder as well as lightening is called thunderstorm.

<strong>The two important characteristics of thunderstorm are following:-</strong>

</br>(i) This can be accompanying with heavy rains or hail.

(ii) This develop hot and humid tropical areas very frequently.

</br>(50) The area which produces a lot of moisture,the place where rising unstable air is present and lifting mechanism are present. This type of area are suitable for Thunderstorm.

<strong>This is because:-</strong>

The air cools as it rises.

Water vapor condenses and forms cumulus clouds.

When condensation occurs, heat (latent heat/energy ) is released and helps the thunderstorm grow.

At some point, condensation high in the cloud (now in the form of water droplets and ice) falls to the ground as rain.

<strong>This is because:-</strong>

</br>(a) The air cools as it rises.

</br>(b) Water vapor condenses and forms cumulus clouds.

</br>(c) When condensation occurs, heat (latent heat/energy ) is released and helps the thunderstorm grow.

</br>(d) At some point, condensation high in the cloud (now in the form of water droplets and ice) falls to the ground as rain.

</br><strong>The precaution should be taken during thunderstorm which are describe as follows:-</strong>

</br>(i) we should not sit down near a window during lightning.

</br>(ii) Do not take shelter under a umbrella as the handle are made with metal.

</br>(iii) If we bare in water during thunderstorm and lightning, then we should get out of the water and fo inside the house etc.

</br><span style="color: #ff0000;"><strong>For More Query about this chapter comment us. Share us through your friend circle.</strong></span></div>`;
export default htmlContent;