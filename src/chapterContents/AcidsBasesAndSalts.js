const htmlContent =`<h1><span style="color: #000080;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 5th Chapter <span style="color: #00ff00;">Acids Bases And Salts</span> Solution</strong></span></h1>
<h3><span style="color: #ff0000;"><strong>Very Short Answer Type Questions:</strong></span></h3>
<div><strong><span style="color: #000000;"> (1) One Natural indicator: Litmus.</span></strong>

One Synthetic Indicator: Phenophthalein.

</br>(2) Litmus.

</br>(3) (a) Litmus Solution: Red

</br>(b) China Rose indicator: magenta (deep pink).

</br>(4) (a) Litmus Solution: Base turn red litmus to blue.

</br>(b) China Rose indicator- Green

</br>(c) Phenolphthalein- Pink colour

</br>(d) Turmeric Paper- Red.

</br>(5) (a) Litmus

</br>(b) Turmeric Paper.

</br>(6) (a) False

</br>(b) False

</br>(c) False

</br>(d) True

</br>(e) True

</br>(7) Base

</br>(8) Then the given solution will be acidic in nature.

</br>(9) It’s a weak bases.

</br>(10) Sulphur dioxide gas and Nitrogen dioxide gas.

</br>(11) Strong Acid: Hydrochloric Acid, Sulphuric Acid.

Weak Acid: Acetic acid, Formic acid.

</br>(12) Strong Bases: Sodium Hydroxide, Potassium Hydroxide.

Weak Bases: Calcium Hydroxide, Ammonium Hydrooxide.

</br>(13) Baking Soda solution or Calamine solution.

</br>(14) Zinc carbonate.

</br>(15) Sodium hydroxide + Hydrochloric acid = Sodium Chloride + Water.

</br>(16) Phenolphthalein indicator.

</br>(17) Sodium Chloride.

</br>(18) (a) Acetic acid, Formic acid.

</br>((b) Hydrochloric acid, Sulphuric acid.

</br>(19) (a) Acids have sour taste.

</br>(b) Bases have bitter taste.

</br>(20) (a) acidic

</br>(b) neutralisation.

</br>(c) Salt and Water.

</br>(d) Salt.

</br>(e) Sodium Chloride

</br>(f) indicator

</br>(g) indicators.
<h3><span style="color: #ff0000;"><strong>Short Answer Type Questions:</strong></span></h3>
(21) An indicator is a dye that changes colour when it is put into an acid or a base.

The indicators are:- Litmus, China Rose, Turmeric.

</br>(22) Litmus solution is obtained from the plant lichen.

Litmus is a water-soluble mixture of different dyes extracted from lichens. It is often absorbed onto filter paper to produce one of the oldest forms of pH indicator, used to test materials for acidity.

</br>(23) This is because soap solution is basic in nature which changes the colour of turmeric in the curry stain to red.

</br>(24)
<table>
<tbody>
<tr>
<td width="319">Acidic</td>
<td width="319">Basic</td>
</tr>
<tr>
<td width="319">Vinegar, Stomach juices, Lemon juice, Toothpaste</td>
<td width="319">Lime water, Milk of magnesia, Ammonia solution</td>
</tr>
</tbody>
</table>
</br>(25) A salt is a substance formed by the reaction of an acid with a base.

Two salts are Sodium Chloride, Sodium sulphate.

</br>(26) The three types of salts are: Neutral salt, Acidic salt, Basic salt.

Neutral salt: sodium chloride.

Acidic salt: Ammonium chloride.

Basic salt: sodium carbonate.

</br>(27)
<table>
<tbody>

<tr style="display:flex;flex-direction:row;"> 
    <td style="width:33.3333%">Acidic salt</td>
    <td style="width:33.3333%">Basic salt</td>
    <td style="width:33.3333%">Neutral salt</td>
</tr>

<tr style="display:flex;flex-direction:row;">
    <td style="width:33.3333%">Sodium chloride </br> Sodium sulphate</td>
    <td style="width:33.3333%">Ammonium chloride </br> Ammonium sulphate.</td>
    <td style="width:33.3333%">Sodium carbonate</td>
</tr>
</tbody>
</table>
</br>(28) This is due to some heat is evolved during this reaction. This heat  increases the temperature of reaction mixture and makes the test tube feel hot.

</br>(29) It will become hot. This is because acid + base releases heat energy.

</br>(30) This is because antacids are a group of mild bases which have no toxic effects on the body . For Basic in nature antacids react with excess acid in the stomach and neutralize it. This give us relief.

</br>(31) This is due to being base calamine solution neutralizes the acidic liquid injected by the an t and cancels it effect.

</br>(32) Calcium limestone or ground limestone (calcium + carbonate=CaCO3), Quick lime or burnt lime (calcium + oxygen=CaO).

</br>(33) Excessive use of Nitrogen fertilizers make the soil basic. Such soils can be corrected by using organic matter.

The decaying organic matter produces acid and neutralises effect of the excess base in the soil.

</br>(34) In order to cure indigestion and get rid of pain we can take bases called antacids.

Antacids used for curing indigestion due to acidity of milk of magnesia.

One antacids are :- Aluminium hydroxide.

</br>(35) (a) Hydrochloric acid is produced in our stomach.

</br>(b) Many problems start in stomach like bloating,nausea,burping etc.

</br>(c) By taking an antacids its effects be cured.

</br>(36) Milk of magnesia is a medicine for indigestion. It contains magnesium hydroxide as antacid.

</br>(37) Burning pain is happened when an ant stings.

If an ant stings a person, then rubbing a milk base like baking soda solution on that particular area of the skin gives relief.

</br>(38) By Litmus paper: If the drink turns blue Litmus Paper to Red then the drink is acidic and hence will be served to first customer who want acidic drink.

If the drink turns red litmus paper blue then the drink is basic and hence will be served to another customer who wants basic drink.

If the drinks shows no change in colour with Litmus Paper that it will be neutral drink thus, it will be served to third customer as neutral drink.

</br>(39) The difference between acids and base are following:-

</br>(a) acids have sour taste whereas bases have bitter taste.

</br>(b) acids are not soapy to touch whereas bases soapy to touch.

</br>(40) Distilled water is neutral by using litmus paper as when it poured on this paper colour is not changed.
<h3><span style="color: #ff0000;"><strong>Long answer Type Questions:</strong></span></h3>
<strong>(41) </strong>The rain which is mixed with higher level of acid then normal called acid rain.

This rain caused by the acidic gases like sulphur-di-oxide, nitrogen di oxide and carbon di oxide..

<u>Damages caused by Acid Rain:</u>

</br>(a) Acid rain makes the ponds, rivers, to acidic that’s why fish and other aquatic animal get killed,

</br>(b) Acid rain damages the metal structure like bridges when it falls on them.

</br>(c) This rain also eats up the leaves of trees gradually.

</br><strong>(42)</strong> Neutral Salt:- Those salts which form a neutral solution on dissolving in water are called neutral solution.

Example:- Sodium Chloride, Sodium Sulphate.

Acidic Salts:- Those salts which form an acidic solution on dissolving in water are called acidic salts.

Example:- Ammonium chloride, ammonium sulphate.

Basic Salts:- Salts which form basic solutions on dissolving in water are called basic salts.

Example:- Sodium carbonate, Sodium hydrogen carbonate.

</br>(43) Acid: A substance which reacts with base to form salt and water called acid.

Two acids are:- Acetic acid, Hydrochloric acid.

We can test the presence of acid in a substance by performing the litmus paper. <strong>For details read the text book.</strong>

</br><strong>(44) Bases: </strong>A substance which can neutralise an acid to form salt and water is called bases.

Two example of bases:- Sodium hydroxide, Potassium hydroxide.

<span style="color: #800080;"><strong>Read the text book for getting the answer of how you can test the presence of base in a substance.</strong></span>

</br><strong>(45) </strong>(a) The reaction in which an acid reacts with a base to form salt and water called neutralisation.

</br>(b) Sodium Hydroxide  (NaOH) + Hydrochloric Acid (HCL) -&gt; Sodium chloride (NaCL) + Water (H<sub>2</sub>O).</div>`;
export default htmlContent;