const htmlContent =`<h1><span style="color: #000080;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 17th Chapter <span style="color: #ff0000;">Forests: Our Lifeline</span> Solution</strong></span></h1>
<h3><span style="color: #0000ff;"><strong>Very Short Answer Type Questions Answers:</strong></span></h3>
<div>(1) Decomposers

</br>(2) Crown.

</br>(3) Two trees are Sal, Mahogany and Two animals are Tiger, Wild Cat.

</br>(4) Canopy.

</br>(5) The topmost layer of forest is called canopy.

</br>(6) Understorey.

</br>(7) Shrub layer.

</br>(8) Herb layer.

</br>(9) Herbs, ferns, grass.

</br>(10) True.

</br>(11) Saprotrophs.

</br>(12) Humus

</br>(13) Canopy.

</br>(14) Wood, Timber, Gum, Rubber, and Honey.

</br>(15) (a) Lowest

</br>(b) Soil

</br>(c) Soil

</br>(d) air and water.

</br>(e) Pollination

</br>(f) decomposers.
<h3><span style="color: #0000ff;"><strong>Short Answer Type Questions Answers:</strong></span></h3>
</br><span style="color: #ff0000;"><strong>(16)</strong></span> The branchy part of a tree above the stem is known as the crown of the tree.

</br><span style="color: #ff0000;"><strong>(17)</strong> </span>The various layer of vegetation in a forest starting from the top downwards are:-

Canopy -&gt; Under Storey -&gt; Shrub Layer -&gt; Herb Layer -&gt; Forest Floor.

</br><strong><span style="color: #ff0000;">(18)</span></strong> When an animals die in a forest it was eaten by other animals called scavengers.

</br><span style="color: #ff0000;"><strong>(19) </strong></span>Trees grows in a natural forest by wind ,water and explosion of trees and by animal.

</br><span style="color: #ff0000;"><strong>(20) The various effect of destruction of forest are:-</strong></span>

</br>(a) Shortage of wood and other forest product.

</br>(b) Disturbed the balance of Carbon Di Oxide and Oxygen in the Atmosphere.

</br>(c) Soil erosion are caused and making the soil infertile to support crops.

</br>(d) Can change the earth’s climate.

</br><span style="color: #ff0000;"><strong>(21) It is necessary for conserve forest because:-</strong></span>

</br>(a) to maintain the supply of wood and other products of forest.

</br>(b) maintain the water cycle in nature and bring sufficient rain.

</br>(c) For prevention of Soil erosion and floods. Etc.

</br><span style="color: #ff0000;"><strong>(22)</strong></span> Paper is made from wood pulp which is produced from the wood. So to make paper many trees have to be cut from the forest. If we are send old newspaper, magazine, unnecessary books to the paper mill for recycling many trees cut will decrease. In this way recycling of paper help in conservation of forest.

</br><span style="color: #ff0000;"><strong>(23)</strong></span> (i) The dead animals and Animal wastes decay and provide nutrients to the forest soil and make it fertile.

</br>(ii) The animals carry out pollination in flowers and also disperse the seeds of plants and trees throughout the forest.

</br><span style="color: #ff0000;"><strong>(24)</strong></span> The reason for large scale cutting down of forest trees are to obtain firewood, timber, making paper, agriculture, land of construction of houses, roads, industries, dams etc.

</br><span style="color: #ff0000;"><strong>(25)</strong></span> The animals which eat dead animals are called scavengers.

The two scavengers are Vulture and Crows.

</br><span style="color: #ff0000;"><strong>(26)</strong></span> There is no waste in a forest because the animals which are die in a forest are eaten by scavengers (ex: vulture, crow etc.) A well as The decomposer organism decompose the dead plant material, dead animal and animal wastes present in the forest into mineral salts , water and carbon di oxide which go into soil and air.

</br><span style="color: #ff0000;"><strong>(27)</strong></span> The example food chain taking place  in a forest are:-

Grass -&gt; Insect (Grass hopper) -&gt; Frog -&gt; Snake -&gt; Eagle.

</br><span style="color: #ff0000;"><strong>(28)</strong></span> The humus contains plants nutrients. This humus mixes with the forest soil and make it fertile.

</br><span style="color: #ff0000;"><strong>(29)</strong></span> The leaves of trees in forest, dead plants, dead animals, animals wastes all are fall on the forest floor. The decomposers organism like fungi, bacteria decompose this organism and to form humus which contains the nutrients. Humus mixes up with soil in the forest and the nutrient present in it fertile it.

</br><span style="color: #ff0000;"><strong>(30) Follow the text book.</strong></span>
<h3><span style="color: #0000ff;"><strong>Long Answer Type Questions Answers:</strong></span></h3>
</br><strong><span style="color: #ff0000;">(31)</span></strong> The microorganisms which break down the dead parts of plants and dead bodies of animals into simple substances such as co<sub>2</sub>, mineral salts etc and which can be re used by plants called decomposers.

The two decomposers are:- Bacteria and fungi.

Decomposers break down the dead parts of plants and dead bodies of animals into simple substances to form humus which contains the nutrients. Humus mixes up with soil in the forest and the nutrient present in it fertile it.

</br><span style="color: #ff0000;"><strong>(32)</strong></span> The roots of trees and plants growing in the forest bind the particles of top soil and hold the soil together firmly. That’s why binding of soil particles, the strong winds, as well as flowing of rainwater are not able to carry away the top soil.

In the absence of trees the soil particles are not bind by the roots of trees. Hence they are carry away by the rain water, floods, strong winds. Thus Forest prevent soil erosion.

</br><span style="color: #ff0000;"><strong>(33)</strong></span> Forest acts as a natural absorber of rainwater and allows it to seep. It helps maintain the water table throughout the year. Forests not only help in controlling floods but also help maintain the flow of water in the streams so that we get a steady supply of water. On the other hand, if trees are not present, rain hits the ground directly and may flood the area around it.

</br><span style="color: #ff0000;"><strong>(34) Forest maintain the balance of carbon di oxide and oxygen in the atmosphere as follows:</strong></span>

The green plants take up carbon di oxide from the atmosphere for making of their food (which are called photosynthesis) and release oxygen during the process of photo synthesis.

Since the green plants and trees of the forest supply oxygen for breathing and respiration. Forest remove carbon di oxide from the atmosphere and put oxygen in the atmosphere Thus forest maintain the balance of oxygen and carbon di oxide.

</br><span style="color: #ff0000;"><strong>(35) Forest are important for us, other animals and the environment :-</strong></span>

</br>(a) Forest provide many useful products.

</br>(b) It maintain the balance between oxygen and CO<sub>2.</sub>

</br>(c) Maintaining water cycle in nature.

</br>(d) Keep the climate cool.

</br>(e) It prevent soil erosion.

</br>(f) Prevent occurrence of floods.

</br>(g) It give food, shelter of animals and forest dwelling peoples.

</br>(h) It keep-s the climate cool.</div>`;
export default htmlContent;