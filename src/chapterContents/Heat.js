const htmlContent =`<h1><span style="color: #000080;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 4th Chapter <span style="color: #ff0000;">Heat</span> Solution</strong></span></h1>
<h3><span style="color: #800080;"><strong>Very Short Answer Type Questions:</strong></span></h3>
<div><span style="color: #0000ff;"><strong>(1) The unit in which temperature is commonly measured is degree celsius.</strong></span></br>

<span style="color: #0000ff;"><strong>(2) The normal temperature of human body on celsius scale is 37 degree celsius.</strong></span></br>

<span style="color: #0000ff;"><strong>(3) The usual temperature range of a laboratory thermometer -10 degree celsius to 110 degree celsius.</strong></span></br>

<span style="color: #0000ff;"><strong>(4) Mercury</strong></span></br>

<span style="color: #0000ff;"><strong>(5) Kink.</strong></span></br>

<span style="color: #0000ff;"><strong>(6) (a) Clinical thermometer</strong></span></br>

<span style="color: #0000ff;"><strong>(b) Laboratory thermometer.</strong></span></br>

<span style="color: #0000ff;"><strong>(7)Celsius scale, Fahrenheit scale.</strong></span></br>

<span style="color: #0000ff;"><strong>(8) 94 degree Fahrenheit to 108 degree Fahrenheit.</strong></span></br>

<span style="color: #0000ff;"><strong>(9) 98.6 degree Fahrenheit.</strong></span></br>

<span style="color: #0000ff;"><strong>(10) Laboratory thermometer.</strong></span></br>

<span style="color: #0000ff;"><strong>(11) Laboratory thermometer.</strong></span></br>

<span style="color: #0000ff;"><strong>(12) Heat can be transferred in three ways- By conduction, By convection and by radiation.</strong></span></br>

<span style="color: #0000ff;"><strong>(13) By conduction.</strong></span></br>

<span style="color: #0000ff;"><strong>(14) Conduction.</strong></span></br>

<span style="color: #0000ff;"><strong>(15) When the hotness is same. </strong></span></br>

<span style="color: #0000ff;"><strong>(16) Insulator.</strong></span></br>

<span style="color: #0000ff;"><strong>(17) Convection.</strong></span></br>

<span style="color: #0000ff;"><strong>(18) Good convector of heat.</strong></span></br>

<span style="color: #0000ff;"><strong>(19) Convection.</strong></span></br>

<span style="color: #0000ff;"><strong>(20) (a) Convection (b) Convection.</strong></span></br>

<span style="color: #0000ff;"><strong>(21) The two type of breeze which blow in coastal areas are- Land breeze and sea breeze.</strong></span></br>

<span style="color: #0000ff;"><strong>(22) Radiation.</strong></span></br>

<span style="color: #0000ff;"><strong>(23) radiation.</strong></span></br>

<span style="color: #0000ff;"><strong>(24) (a) Radiation</strong></span></br>

<span style="color: #0000ff;"><strong>(b) Conduction</strong></span></br>

<span style="color: #0000ff;"><strong>(c) Convection.</strong></span></br>

<span style="color: #0000ff;"><strong>(25) Radiation.</strong></span></br>

<span style="color: #0000ff;"><strong>(26) Heat from the sun reach to the earth by radiation.</strong></span></br>

<span style="color: #0000ff;"><strong>(27) Radiation.</strong></span></br>

<span style="color: #0000ff;"><strong>(28) Conduction.</strong></span></br>

<span style="color: #0000ff;"><strong>(29) (a) Shiny white surface- better reflector of heat.</strong></span></br>

<span style="color: #0000ff;"><strong>(b) Dull black surface: better absorber of heat.</strong></span></br>

<span style="color: #0000ff;"><strong>(30) Fill in the following blank with suitable words:</strong></span></br>

<span style="color: #0000ff;"><strong>(a) Conduction.</strong></span></br>

<span style="color: #0000ff;"><strong>(b) Conduction</strong></span></br>

<span style="color: #0000ff;"><strong>(c) Plastic and Wood.</strong></span></br>

<span style="color: #0000ff;"><strong>(d) Poor, Good</strong></span></br>

<span style="color: #0000ff;"><strong>(e) air</strong></span></br>

<span style="color: #0000ff;"><strong>(f) temperature.</strong></span></br>

<span style="color: #0000ff;"><strong>(g) Celsius.</strong></span></br>

<span style="color: #0000ff;"><strong>(h) Clinical thermometer.</strong></span></br>

<span style="color: #0000ff;"><strong>(i) radiation.</strong></span></br>

<span style="color: #0000ff;"><strong>(j) Convection.</strong></span></br>

<h3><span style="color: #800080;"><strong>Short Answer Type Questions:</strong></span></h3>
<strong>(31) Frying pan made of metal whoeever its aluminium or stainless steel but the handle of this is made of plastic or wood because plastic or wood is a poor conductor of heat so that we can lift it safely from its handle.</strong></br>

<strong>(32) Plastic is a poor conductor of heat. So the plastic handle prevents the heat from the hot cooking utensil reaching our hand so that we can lift the hot cooking utensil safely from its handle.</strong></br>

<strong>(33) This is due to as soon as we take out the bulb of the laboratory thermometer from our mouth the level of mercury in its tube will start falling quickly. So this will give the wrong value of the body temperature.</strong></br>

<strong>(34) This is because the clinical thermometer has a kink. The kink is to prevent the back flow of mercury into the thermometer bulb when the thermometer is removed from the patient's mouth.</strong></br>
</br>
<strong>(35) Transfer of heat from the hot part of a material to its colder part is called conduction.</strong>

<strong>Example: A cold metal spoon dipped in a hot cup of tea gets heated by conduction.</strong></br>

<strong>(36) Those material which allow hit to pass through them easily are called good conductor of heat.</strong>

<strong>Example: Steel, Stainless steel.</strong></br>

<strong>(37) Those material which not allow hit to pass through them easily are called poor conductor of heat.</strong>

<strong>Example: Plastic, Wood.</strong></br>

<strong>(38) </strong>
<table>
<tbody>
<tr>
<td width="319"><strong>Good Conductor of heat</strong></td>
<td width="319"><strong>Poor conductor of heat / insulator</strong></td>
</tr>
<tr>
<td width="319">Copper, Stainless steel, Aluminium</td>
<td width="319">Wood, Plastic, Air, Paper, Glass, Thermocol, Rubber</td>
</tr>
</tbody>
</table>
<strong>(39) (a) Solid- Conduction.</strong></br>
</br>
<strong>(b) Liquid- Convection.</strong>

<strong>(c) Gas- Convection.</strong></br>

<strong>(d) Empty space (or vaccum)- Radiation.</strong></br>

<strong>(40) This is because Stainless steel is good conductor of hit as well as Thermocol is not because it is a poor conductor of hit.</strong></br>

<strong>(41) This is due to the temperature of human body normally does not go below 35 degree celsius or 42 degree celsius.</strong></br>

<strong>(42) The kink prevents the mercury level in the thermometer tube from falling on its own.</strong></br>

<strong>(43) The temperature of an object is the degree of hotness or coldness of the object.</strong></br>

<strong>(44) The difference between clinical thermometer and laboratory thermometer are:</strong>
<table>
<tbody>
<tr>
<td width="319"><strong>I) The clinical thermometer has A VERY SHORT temperature range 35 degree celsius to 42 degree celsius.</strong></td>
<td width="319"><strong>I) Laboratory thermometer has very large temperature range -10 degree celsius to 110 degree celsius.</strong></td>
</tr>
<tr>
<td width="319"><strong>II) The clinical thermometer has kink.</strong></td>
<td width="319"><strong>II) Laboratory thermometer has no kink.</strong></td>
</tr>
</tbody>
</table>
<strong>The similarities between clinical thermometer and laboratory thermometer are :- both are uses to measure the temperature as well as both have mercury.</strong></br>

<strong>(45) This is due to the sun is very far away from the earth and there is mainly empty space between sun and earth.</strong>
<h3><span style="color: #800080;"><strong>Long Answer Type Question:</strong></span></h3>
<span style="color: #333399;"><strong>(46) Wool is a poor conductor of heat, for this reason stop the flow of heat from our warm body to the cold surroundings.</strong></span>

<span style="color: #333399;"><strong>Wearing more layers of clothing Will have a layer of air trapped in between them. And this extra layer of trap air will prevent our body heat from going away to the cold air more efficiently and hence keep us more warm. The single cloth does not have a extra layer of air in it.</strong></span></br>

<span style="color: #333399;"><strong>(47) The temperature used for measuring the temperature of human body is called clinical thermometer.</strong></span>

<span style="color: #333399;"><strong>The range of clinical thermometer is between 35 degree Celsius to 42 degree celsius.</strong></span>

<span style="color: #333399;"><strong>A clinical thermometer can not be used to measure high temperatures because it has been designed to measure only human body temperature which is very short range from 35 degree Celsius to 42 degree Celsius.</strong></span></br>

<span style="color: #333399;"><strong>(48) <span style="color: #ff0000;">Image</span></strong></span></br>

<span style="color: #333399;"><strong>(49) <span style="color: #ff0000;">Image</span></strong></span></br>

<span style="color: #333399;"><strong>(50) The breeze blowing from the sea towards the land is called sea breeze.</strong></span>

<span style="color: #333399;"><strong>The breeze blowing from the land towards the sea is called land breeze.</strong></span></br>
</br>
<span style="color: #333399;"><strong>(51) (a) We prefer white clothes during summer because white clothes absorb less heat from the sun. White clothes reflect most of the sun's heat rays which fall on them and keep us cool and comfortable.</strong></span></br>

<span style="color: #333399;"><strong>(b) We prefer black clothes in winter because black clothes is good absorber of heat and hence it will absorb maximum heat radiations coming from the sun.</strong></span></br>

<span style="color: #333399;"><strong>(52) An instrument which measure the temperature is called thermometer.</strong></span>

<span style="color: #333399;"><strong>Clinical thermometer is basically used by Doctor and the nurses.</strong></span></br>

<strong>(53)</strong> (a) The box of solar cooker is painted black from inside. This is because a black surface is a very good absorber of heat and it will absorb maximum heat radiations coming from the sun.</br>

(b) In places of hot climate it is advised that the outer walls of the houses be painted white because, a house painted white or with light colours absorbs very little sun’s heat rays. This keeps the house cool in the hot days of summer.</br>

<strong>(54) </strong>Convection is the transfer of heat from the hotter parts of a liquid (or gas) to its colder parts by the movement of the liquid (or gas) itself.

</br><strong>Explain with Example:</strong></br>

When a beaker containing water is kept over a burner water at the buttom of beaker gets heated, expands and becomes lighter. This hot water rises upwards and carries heat alongwith it. This cold water from above sinks downwards to take the place of hot rising water. This cold water from above sinks downwards to take the place of hot rising water . This water then gets heated by the burner and also rises upwards carrying its heat upward. And more cold water sinks downwards.

The transfer of heat by convection cannot take place in solids because the particles in the solids are fixed at a place and hence cannot move about freely.

</br>(55) <strong>Radiation: </strong>The hot electric bulb transfers its heat to our hand held below it by the process called radiation.

</br><strong>Two example of radiation:</strong>

</br>Example: (i) When we come out in the sunshine, we feel hot.

</br>(ii) When we stand next to a burning fire, we can feel the heat of the fire falling on our face. The heat is transferred from the fire to our face by the process of radiation.

The name of invisible heat rays which transfer heat by radiation is called Infrared radiation.

</div>`;
export default htmlContent;
/*<a href="https://www.netexplanations.com/lakhmir-singh-manjit-kaur-class-7-solution/" target="_blank" rel="noopener noreferrer"><strong>Lakhmir Singh Class 7 Total Book Solution Link</strong></a>

<a href="https://www.netexplanations.com/lakhmir-singh-manjit-kaur-class-8-solution/" target="_blank" rel="noopener noreferrer"><strong>Lakhmir Singh Class 8 Total Book Solution Link</strong></a>

<a href="https://www.netexplanations.com/lakhmir-singh-manjit-kaur-science-book-solution/" target="_blank" rel="noopener noreferrer"><strong>Lakhmir Singh Class 9 Total Book Solution Link</strong></a>*/