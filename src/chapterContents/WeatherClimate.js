const htmlContent =`<h1><span style="color: #000080;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 7th Chapter <span style="color: #ff0000;">Wheather Climate And Adaptations Of Animals To Climate</span> Solution</strong></span></h1>
<h3><span style="color: #0000ff;"><strong>Very Short Answer Type Questions:</strong></span></h3>
<div>(1) Sun

</br>(2) We usually take umbrella or raincoat on rainy day, We usually wear cotton cloth in summer season.

</br>(3) Humidity

</br>(4) Rain gauge.

</br>(5) millimeters (mm)

</br>(6) Weather

</br>(7) (a)Very hot and wet- Kerala.

</br>(b) Hot and dry- Rajasthan.

</br>(c) Moderately hot and wet- Jammu Kashmir.

</br>(d) Wet- North-East India.

</br>(8) Assam

</br>(9) Rajasthan

</br>(10) Polar bear, Penguin

</br>(11) Musk oxen and Reindeers.

</br>(12) Penguin

</br>(13) Bharatpur in Rajasthan, Sultanpur in Haryana.

</br>(14) Five types of animal which live in tropical rainforest are :- Lions, Tiger, Monkeys, Gorillas and Elephants.

</br>(15) Lion, Tiger.

</br>(16) Siberian crane.

</br>(17) Its silver-white Mane which surrounds its head from the cheeks down to its chin.

</br>(18) (a) False

</br>(b) False

</br>(19) Hot and Wet

</br>(20) (a) Climate

</br>(b) Weather

</br>(c) Hot and dry

</br>(d) Polar Region and Tropical Region

</br>(e) Shorter

</br>(f) Western Ghats and Assam
<h3><span style="color: #0000ff;"><strong>Short Answer Type Questions:</strong></span></h3>
</br>(21) In morning the minimum temperature occur and in Afternoon the maximum temperature occur.

</br>(22) We feel so miserable in the afternoon because the maximum temperature of the day is reached in that time whereas the minimum temperature occur in morning so we feel so comfortable.

</br>(23) Maximum and Minimum temperature; On which time moon will set; Sky will clear or not, Maximum Humidity record etc.

</br>(24) An instrument by which rainfall is measured is called rain gauge.

Rainfall is measured by this instrument.

</br>(25) By having some special features such as white colour, thick layer of fur, thick skin, a layer of fat under the skin, strong sense of smell, and big paws.

</br>(26) The white colour helps the polar bear to catch its pray. The white colour also protect it from its predators.

</br>(27) (a) Polar bear has white colour which matches with its surroundings; (b) Polar bear has two thick layers of fur on its body.

</br>(28) The two thick layer of fur keep the polar bear warm in extremely cold climate.

Thick layer of fat also protect the polar bear from extreme cold.

</br>(29) This features help the polar bear in locating and catching its pray for food.

</br>(30) This feature helps the polar bear to spread the weight of its body on snow which prevents it from sinking into snow and make it walk on snow easily.

</br>(31) This feature helps the polar bear to catch its pray like seal very easily.

</br>(32) Its trunk, Its tusks, Large ears, Large feet and round helps the elephant living in the tropical rainforest adapt itself.

</br>(33) Tusks are big and long pointed teeth which comes out from closed mouth of the elephant.

Elephants uses the tusks to tear off the bark of trees which it eats as food. And also use it for fighting their enemies and protect itself.

</br>(34) This feature help the elephant to hear even the slightest sound made by its predators behind him so that it can hear the danger and run away the safety place.

</br>(35) Red eyed from has developed sticky pads on its feet. This feature helps it to climb easily on the trees on which it lives.

</br>(36) This feature help the Toucan for getting the otherwise unreachable fruits at the ends of very thin branches of trees.

</br>(37) Monkeys are expert climbers; It have long and strong gripping tails which they use for grasping branches of trees, Very good eyesight.

</br>(38) The other name of lion tailed macaque is Beard ape.

The lion tailed macaque lives in Western Ghats in India.

</br>(39) Good climber, Good eyesight help the lion tailed macaque to live and survive on trees in the tropical rainforest.

</br>(40) Black and White in colour, Thick skin and Layer of fat below its skin, streamlined body, flipper like wings and webbed feet help the penguin to live and survive in on trees.

</br>(41) Two features are Thick skin and Layer of fat under its skin which protects the penguin in extreme cold regions.

</br>(42) Penguin huddle together to keep themselves warm.

</br>(43) Streamlined body, flipper like wings and webbed feet help the penguin to swim.

Being a good swimmer helps penguin in catching fish as pray.

</br>(44) <u>Migration of Bird:- </u>is an adaptation to escape the harsh and cold conditions of their normal habitat in winter so as to survive.

Birds are migrate to escape the harsh and cold condition.

</br>(45) The birds which migrate very cold regions to warmer regions and go back after end the winter over are called migratory bird.

One migratory bird are Siberian Crane.

</br>(46) The tropical regions have hot and humid climate.

An important feature of the tropical regions of earth is the tropical rainforest.

</br>(47) Tropical rainforest found in Assam, Maharashtra, Karnataka, Tamilnadu and Kerala.

</br><u>(</u>48) Highly favorable climate such as continuous warmth and rain support tropical rainforest a wide variety of plants and animal.

</br>(49) To escape the extremely cold winter in Siberia they are do so.

</br>(50) Two adaptation of big cats are following:-

</br>(i) Highly developed sense of smell.

</br>(ii) Run very fast.
<h3><span style="color: #0000ff;"><strong>Long answer Type Questions:</strong></span></h3>
</br>(51) The day to day condition of the atmosphere at a place and time with respect to the temperature, pressure, wind speed, humidity, wind direction, clouds, sunshine, rainfall is called weather.

The elements which determines the weather of a place are temperature, pressure, humidity, rainfall, sunshine, clouds, wind speed or direction etc.

</br>(52) The hit of the sun and the effects it has on the atmosphere produces weather.

<u>Why weather changes so frequently:-</u>

This is because the earth atmosphere condition keeps changing frequently for the reason of various effects produced by the sun’s hit.

</br>(53) The average weather taken over 25 years is called the climate of that place. If the temperature of a place is high then we say that the climate of that place is hot and is there is also heavy rainfall on most of the days at that place then we say that climate of that place is hot and wet.

The four major type of climate are following:-
<ol>
 	<li>a) Polar climate.</li>
 	<li>b) Temperate climate</li>
 	<li>c) Tropical climate</li>
 	<li>d) desert climate.</li>
</ol>
</br>(54) There are various factors which affect the climate. They are:- Temperature and rainfall; Its altitude; as well as location with respect to the sea.

The difference between Srinagar and Thiruvananthapuram in location as Srinagar is at high altitude on a mountain in the Northern part of India whereas Other is situated on flat ground at sea level of South part of India. One more that Srinagar is quite far away from the equator whereas Thiruvananthapuram is nearer to the equator.

<u>The difference of climate between Srinager and Thiruvananthapuram are following:-</u>

(i) In Srinagar the temperature throughout the year neither very high nor very low. And the rainfall also the same whereas In  Thiruvananthapuram the temperature throughout the year is quite high and the rainfall also the same.

</br>(55) The polar regions of the earth have an extremely cold climate.

The climate of the polar regions is very cold. For six moth the sun does not set the poles whereas the next six month the sun does not rise. During winter in polar regions the temperature can be low as -37 degree Celsius.

The five countries are :- Canada, Green land, Iceland, Norway, Sweden.

</br>(56) The presence of specific body features which enable an animal or plant to live in a particular habitat called adaptation.

The animals living in two types of habitat – (i) Polar regions (ii) Tropical rainforests.

Adaptation in Polar Regions:-

(a) White colour which matches with its surroundings.

(ii)Thick layers of fur on its body.

(iii) Thick layer of fat under its skin.

(iv) Rounded body and small ears.

(v) Good Swimmer

(vi) Strong sense of smell.

</br>(57) (a) Tropical Rainforest:- are hot moist woodland found near the equator.

The five countries where tropical rainforest are found are India, Malaysia, Indonesia, Brazil, Kenya.

(b) A skin colour which helps them to mix up the surroundings and hide in order to catch its pray and protects themselves from its enemy.

Running very fast.

Development of trunk.

Gripping tails.

Long and large beaks.

Bright colours and Loud voices etc help the animal live and survival in tropical rainforest.

</br>(58) (a) We find animals of certain kind living in particular climatic conditions because every living organism is adapted to live in the particular habitat. Every living thing develops their feature according to the area they live in.

Example: penguins have thick skin to protect themselves from cold. Webbed feet to swim in water. It will not be able to survive in the hot climate of tropical regions as they developed thick skin for the protection from cold not from the hot and humid temperature.

(b)
<table>
<tbody>
<tr>
<td width="319">Polar Regions</td>
<td width="319">Tropical Rainforest</td>
</tr>
<tr>
<td width="319">Musk oxen, reindeer, Penguin, Seal, Siberian crane, Arctic fox</td>
<td width="319">Elephant, Lion tailed Macaque, Toucan, Red eyed frog,</td>
</tr>
</tbody>
</table>
</br>(59) The difference between weather and climate are following:-

(i) The day to day condition of the atmosphere at a place and time with respect to the temperature, pressure, wind speed, humidity, wind direction, clouds, sunshine, rainfall is called weather. Whereas,

The average weather taken over 25 years is called the climate of that place.

(a) Jammu and Kashmir- Tropical Climate.

(b) Kerala- Temperate Climate.

(c) North East India- Polar Climate.

(d) Rajasthan- Desert Climate.

</br>(60) (a) It help them to escape extremely cold winter condition.

One bird and mammal which migrate seasonally are Siberial crane and Reindeer respectively.

</br>(b) This is because of the trunk it has a strong sense of smell.</div>`;
export default htmlContent;