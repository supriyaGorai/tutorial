const htmlContent =`<h1><span style="color: #000080;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 13th Chapter <span style="color: #ff0000;">Motion And Time</span> Solutions</strong></span></h1>
Lakhmir Singh and Manjit Kaur Science solution: Motion And Time Chapter 13. Here you get easy solutions of Lakhmir Singh and Manjit Kaur Science solution Chapter 13. Here we give Chapter 13 all solution of class 7. Its help you to complete your homework.

Board – CBSE

Text Book - SCIENCE

Class – 7

Chapter – 13
<h3><span style="color: #ff0000;"><strong>Very Short Answer Type Questions:</strong></span></h3>
<div><strong>(1) M/S.</strong></br>

<strong>(2) Km/Hr.</strong></br>

<strong>(3) Speed.</strong></br>

<strong>(4) Km/Hr.</strong></br>

<strong>(5) (a) Odometer.</strong></br>

<strong>(b) Speedometer.</strong></br>

<strong>(6) (a) False</strong></br>

<strong>(b) False</strong></br>

<strong>(c) True</strong></br>

<strong>(d) False</strong></br>

<strong>(e) True</strong></br>

<strong>(f) True</strong></br>

<strong>(g) False</strong></br>

<strong>(7) Bar graph, Pie chart.</strong></br>

<strong>(8) Time</strong></br>

<strong>(9) Second.</strong></br>

<strong>(10) Hour, Minute, Second.</strong></br>

<strong>(11) 3600</strong></br>

<strong>(12) Sundial, Water clock.</strong></br>

<strong>(13) Simple Pendulum.</strong></br>

<strong>(14) Month, Day</strong></br>

<strong>(15) Bob.</strong></br>

<strong>(16) Periodic Motion.</strong></br>

<strong>(17) Quartz Crystal.</strong></br>

<strong>(18) Clocks, Watches.</strong></br>

<strong>(19) (a) 1 Second</strong></br>

<strong>(b) 1/10<sup>th</sup> Second.</strong></br>

<strong>(20) Micro Second.</strong></br>

<strong>(21) A quartz clock.</strong></br>

<strong>(22) A quartz clock.</strong></br>

<strong>(23) Speed = Distance covered / Time; 10m / 40 s = 0.25 m/s.</strong></br>

<strong>(24) 0.2 hour</strong></br>

<strong>(25) An instrument on a vehicles dashboard which indicates the speed of the vehicle when it is running is called speedometer.</strong></br>

<strong>(26) An instrument which is used for measuring the distance travelled by a vehicle.</strong></br>

<strong>(27) when the object is stationary.</strong></br>

<strong>(28) Speed of an object is the distance travelled by it in unit time.</strong></br>

<strong>(29) Speed = Distance Covered / Time Taken.</strong></br>

<strong>(30) (a) Speed</strong></br>

<strong>(b) no-uniform</strong></br>

<strong>(c) uniform</strong></br>

<strong>(d) straight</strong></br>

<strong>(e) month</strong></br>

<strong>(f) day</strong></br>

<strong>(g) periodic</strong></br>

<strong>(h) simple pendulum</strong></br>

<strong>(i) time-period</strong></br>
<h3><span style="color: #ff0000;"><strong>Short Answer Type Questions Answers:</strong></span></h3>
<strong>(31) (a) Periodic</strong></br>

<strong>(b) Rectilinear</strong></br>

<strong>(c) Periodic</strong></br>

<strong>(d) Periodic</strong></br>

<strong>(e) Rectilinear motion.</strong></br>

<strong>(32) (a) A bar graph is a diagram which shows information as bars of different heights.</strong>

<strong>Example:- A bar graph showing the information about the runs scored in six overs of a cricket match.</strong></br>

<strong>(b) A pie chart is a kind of graph or diagram which shows the percentage composition of something in the form of slices of a circle.</strong>

<strong>Example: A pie chart showing the composition of air.</strong></br>

<strong>(33) (a) Rectilinear motion.</strong></br>

<strong>(b) Circular motion</strong></br>

<strong>(c) Periodic motion</strong></br>

<strong>(d) Rectilinear motion</strong></br>

<strong>(e) Rotational motion.</strong></br>

<strong>(35) The distance between Delhi and Amritsar is = Speed x Time Taken</strong>

<strong>=&gt; 75 km/h x 6 h</strong>

<strong>=&gt; 450 km.</strong></br>

<strong>(36) Time taken = Distance Travelled / Speed</strong>

<strong>= 270 km / 60 km/hr</strong>

<strong>= 4.5 hr.</strong></br>

<strong>(37) Distance Covered = 1 metre = 100 cm</strong>

<strong>Time taken = 10 second</strong>

<strong>Speed = 100 cm / 10 s = 10 cm/s</strong></br>

<strong>(38) Speed = 120 km</strong>

<strong>Time taken = 1 minute</strong>

<strong>= 1 / 60 hr.</strong>

<strong>Distance Travelled = 120 x 1/ 60</strong>

<strong>= 2 km.</strong></br>

<strong>(39) Speed = 45 km /h</strong>

<strong>Time taken = 20 minutes</strong>

<strong>= 20 / 60 = 1 / 3 h.</strong>

<strong>Distance Travelled = 45 x 1 / 3</strong>

<strong>= 15 km.</strong></br>

<strong>(40) Total Distance = 240 km</strong>

<strong>Time taken = 4 hours</strong>

<strong>Speed = 240 km / 4 hr</strong>

<strong>= 60 km / hr.</strong></br>

<strong>(41) Distance = 5 m 20 cm</strong>

<strong>= 500 + 20 = 520 cm</strong>

<strong>Time taken = 4 sec</strong>

<strong>Speed = Distance travelled / Time taken</strong>

<strong>= 520 cm / 4 sec = 130 cm/sec</strong></br>

<strong>(42) Train speed = 60 km/hr</strong>

<strong>=60/60 km/min</strong>

<strong>=60/60x60 km/sec</strong>

<strong>=60x1000/60x60 m/sec</strong>

<strong>=16.66 m/sec.</strong></br>

<strong>(43) Speed of the racing car=60m/s</strong>

<strong>1m =1/1000 km</strong>

<strong>1s 1/3600 h</strong>

<strong>Speed of car in km /h =(60×3600)÷1000</strong>

<strong>=216km/h</strong></br>

<strong>(44) </strong>(a) Uniform motion.

(b) Non Uniform motion.

<strong>(45) </strong>(a) Circular motion

(b) Rotational motion

(c) Periodic motion

(d) Periodic motion.
<h3><span style="color: #ff0000;"><strong>Large Answer Type Questions:</strong></span></h3>
</br><strong>(46) Uniform Motion:- </strong>an object moving along a straight line path is said to have uniform motion is its speed remains constant.

Example: The motion of a car running at a constant speed.

</br><strong>Non Uniform Motion:- </strong>An object moving along a straight line path is said to have non uniform motion if its speed keeps changing.

Example: The motion of a train starting from a railway station.

</br><strong>(50) Simple Pendulum:- </strong>A simple pendulum consists of a small metal ball suspended by a long thread from a rigid support such that the bob is free to swing back and forth.

Time Period of a Simple Pendulum:- The time taken by pendulum bob to make one complete oscillation is called time period of the pendulum.

</br>(b) Time taken for 20 complete oscillations = 36 s.

Time taken for 1 oscillation = 36 s / 20 = 1.8 s

Thus, the time period of this pendulum is 1.8 seconds.</div>`;
export default htmlContent;