const htmlContent =`<h2><span style="color: #0000ff;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 16th Chapter <span style="color: #ff0000;">Water: A Precious Resource</span> Solution </strong></span></h2>
Lakhmir Singh and Manjit Kaur Science solution: Water: A Precious Resource Chapter 16. Here you get easy solutions of Lakhmir Singh and Manjit Kaur Science solution Chapter 16. Here we give Chapter 16 all solution of class 7. Its help you to complete your homework.
<ul>
 	<li><span style="color: #ff0000;"><strong>Board –</strong></span> CBSE</li>
 	<li><span style="color: #ff0000;"><strong>Text Book -</strong> </span>SCIENCE</li>
 	<li><span style="color: #ff0000;"><strong>Class –</strong> </span>7</li>
 	<li><span style="color: #ff0000;"><strong>Chapter –</strong> </span>16</li>
</ul>
<h3><span style="color: #ff0000;"><strong>Very Short Answer Type Questions Answers:</strong></span></h3>
<div>(1) Solid, Liquid, Gas.

</br>(2) (a) Water Vapour (b) Ice.

</br>(3) Drip Irrigation.

</br>(4) (a)False

</br>(b) False

</br>(c) True

</br>(d) True

</br>(e) False

</br>(5) Rain

</br>(6) Bawris.

</br>(7) Aquifer.

</br>(8) Rain.

</br>(9) Water Cycle.

</br>(10) 50 Liter.

</br>(11) Water table will fall drastically.

</br>(12) (a) Solid, Liquid, Gas.

</br>(b) 71%

</br>(c) Infiltration

</br>(d) Hand Pump, Tube wells

</br>(e) Aquifer

</br>(f) Groundwater

</br>(g) aquifer

</br>(h) water

</br>(i) mismanagement

</br>(j) recharge

</br>(k) rain water harvesting

</br>(l) taps
<h3><span style="color: #ff0000;"><strong>Short Answer Type Questions Answers:</strong></span></h3>
</br>(13) The earth appears blue from space as because about 71 percent of earths surface is covered with water.

</br>(14) The upper level of water under the ground which occupies all the spaces in the soil and rocks is called water table.

One factor which rises water table are deforestation.

</br>(15) The various factor which affect the water table are Increase in population, Industrial and agricultural activities, Scanty rainfall, Deforestation, Decrease in the effective area for seepage of rainwater.

</br>(16) The underground layer of soil and permeable rocks in which water collects under the ground is called an aquifer.

The upper level of aquifer known as

</br>(17) The water found below the water table is called water table.

The main source of ground water is rain.

</br>(18) Drip irrigation is a technique of watering plants by making use of a system of narrow pipes or tubes with small holes, which deliver water directly at the base of the plants.

The drip irrigation minimizes the use of water in agriculture.

</br>(19) The plants will limp and ultimately dry up if the potted plants are not watered even for a few days.

</br>(20) Water is very essential for us similarly plants also need water for their survival. Water helps to transport the nutrients to other parts of the plant. Water also plays very important role in carrying various processes in plants. Therefore scarcity of water will have adverse effect on plants. It may cause death of a plant.

</br>(21) There are various ways to stop the wastage of water at home. They are given below:-

</br>(i) If taps are liquid, repaired immediately.

</br>(ii) Take bath by filling water in bucket no directly under taps or take shower.

</br>(iii) Use the water from washing rice, pulses, vegetables etc for watering the plants at home.

</br>(iv) Do not use full flush for clean the toilet where half flush is sufficient.

</br>(22) If it will happen then I am used drip irrigation technique. Drip irrigation is a technique of watering plants by making use of a system of narrow pipes or tubes with small holes, which deliver water directly to the base of the plants.

One more that The wastage of water during the time of water plants and through the leaking taps are prevented.

</br>(23) When it rains the rain water seeps into the ground and the groundwater is recharged.

</br>(24)

</br>(25) 22 March celebrated as world water day.

It is celebrated to attract the attention of everybody towards the importance of conserving water.
<h3><span style="color: #ff0000;"><strong>Large Answer Type Questions Answers:</strong></span></h3>
</br>(26) (a) At many places water is supplied through pipes by the water supply department. In where by the way of supplied water we can see mismanagement of water. Like example, some of places pipes are leaked through which water is gushes out. It is our duty to call the water supply department and fixed it quickly. Sometime dirty water also mixed with the drinking water through the leaked part of pipes. It is also a part of mismanagement of water of Water supply department.

</br>(b) <u>At the Individual level:-</u> All of us knowingly and unknowingly waste a lot of water while brushing teeth, cleaning utensils, taking bath, usage of water at the toilet, etc.

</br>(27) The two steps which can be taken for proper management of water are:

&lt;a.&gt; Rainwater harvesting:- One way of increasing the availability of water from its shortage is rainwater harvesting. By making rainwater seep into the ground more efficiently by constructing percolation pits and recharge wells so as to recharge groundwater. This water can be pumped out in the time of water needed.

&lt;b&gt; Drip Irrigation:- This is the another process for proper management of water. Drip irrigation is a technique of watering plants by making use of a system of narrow pipes or tubes with small holes, which deliver water directly to the base of the plants.  Drip irrigation process gives water directly to the root zone of plants and it will also pour water slowly which will beneficial for absorbing water of plants without any wastage.

</br>(28) <u>Increasing Population is Responsible for Depletion of Water Table:</u>

As a result of increasing population, all the facilities such as houses, shops, roads, offices, pavements, industries, Agriculture activities etc. increase to fulfil the increasing demands. This, in turn, decreases the open area for seepage of water into the ground.

</br>(29) <u>Increasing Industrial Activities are Causing the Depletion of Water Table:-</u>

There are lots of industries in the world which are produce various thing for our needed. Every industries use water at some stage during manufacturing or producing time. As the population increase the demands for various industrial goods are also increase.  That’s why the number of industries are also increase. Water used by the most of industries are pumped out from the ground. As the number of industries are increasing the use of water is also increasing which are causing the depletion of water table.

</br>(30) <u>Agricultural Activities responsible for Depletion of Water Table:-</u>

A majority of farmers in India depend upon rains for irrigating their crops. Irrigation system such as reservoirs, dams exist in some places but this irrigation system get water from rain water. But at the time of irregular rainfall those farmers pumps the ground water  for irrigation their crops. As the population increase the demand of agricultural food is also increase. For fulfilling those a lot of demand agricultural activity are increase and the use of ground water is also increase. This are the depletion of water table.</div>`;
export default htmlContent;