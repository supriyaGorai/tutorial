const htmlContent =`<h2><span style="color: #0000ff;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 9th Chapter <span style="color: #ff0000;">Soil</span> Solution</strong></span></h2>
Lakhmir Singh and Manjit Kaur Science solution: Soil Chapter 9. Here you get easy solutions of Lakhmir Singh and Manjit Kaur Science solution Chapter 9. Here we give Chapter 9 all solution of class 7. Its help you to complete your homework.
<div class="intro-text">
<ul>
 	<li><strong>Board – CBSE</strong></li>
 	<li><strong>Text Book - SCIENCE</strong></li>
 	<li><strong>Class – 7</strong></li>
 	<li><strong>Chapter - 9</strong></li>
</ul>
</div>
<h3 style="text-align: center;"><span style="text-decoration: underline;"><strong>Biology Chapter 9 (Soil) Exercise Solution - Lakhmir Singh Manjit Kaur Book</strong></span></h3>
&nbsp;
<h3><span style="color: #ff0000;"><strong>Very Short Answer Type Questions Answers:</strong></span></h3>
<div>(1) Air

</br>(2) Water

</br>(3) It supplies oxygen to organisms which lives in soil.

</br>(4) (a) Clayey soil

</br>(b) Sandy soil

</br>(5) Clayey soil and Loamy soil.

</br>(6) Loamy soil is the best soil for growing wheat crop.

</br>(7) Loamy soil.

</br>(8) Loamy soil.

</br>(9) Sandy-loam soil is suitable for growing cotton crop.

</br>(10) The different types of rock particles present in soil are Clay, Silt, Sand and Gravel.

</br>(11) Silt.

</br>(12) Clay &lt; Silt &lt; Sand &lt; Gravel.

</br>(13) Humus.

</br>(14) Weathering.

</br>(15) Humus.

</br>(16) Brown soil profile.

</br>(17) (a) Clayey soil

</br>(b) Sandy soil.

</br>(18) Loamy soil.

(19) Loam soils.

</br>(20) The collective name for various layer of soil is soil profile.

</br>(21) A horizon (Top soil) , B-Horizon (Sub soil) and C-Horizon (Sub stratum).

</br>(22) Top Soil / A-Horizon of soil contains a lot of humus.

</br>(23) Top Soil / A-Horizon is most fertile.

</br>(24) Bed Rock (Or Parent Rock).

</br>(25) Five living organism live in soil are: Earthworm, Ant, Snake, Rabbit, Moles etc.

</br>(26) (a) Sandy soil.

</br>(b) Clayey soil.

</br>(27) I will use those type of bags made of paper, cloth, jute etc.

</br>(28) (a) Three basic types of soils are Sandy, Clayey and Loamy.

</br>(b) Soil erosion.

</br>(29) (a) The two factors are wind and water.

</br>(b) The roots of the trees binds the soil together and prevents the soil erosion.

</br>(30) (a)- soil

</br>(b)- horizon

</br>(c)- top

</br>(d)- B

</br>(e)- profile

(f)- soil

</br>(g)-loose

</br>(h)-wind

</br>(i)- erosion

</br>(j)- polythene

</br>(k)- mL/min
<h3><span style="color: #ff0000;"><strong>Short Answer Type Questions answers:</strong></span></h3>
</br>(31) (a) After poured 0on cemented floor we absorbed that the water poured on the cemented floor are flows down, it is not absorbed by the cemented floor.

</br>(b) We will see that soil absorbs the water quite rapidly.

</br>(32) When some water is poured on soil it gets absorbed because it is porous, that’s mean having tiny pores in it. Whereas same water flows down on the cemented floor as because it is not porous.

</br>(33) Sandy soil provides more air to plant roots because clayey soil is not able to trap- enough air for the roots of plants.

</br>(34) The important properties of soil are:-

</br>(a) Soil contains air.

</br>(b) Soil contain s water.

</br>(c) Soil can absorb water.

</br>(35) Loamy soil is very useful for the crops because it is rich in humus and very fertile.

</br>(36) It is not possible to make pots with sandy soil because it5 is not sticky.

</br>(37)
<table>
<tbody>
<tr style="display:flex;flex-direction:row;">
<td style="width:33.3333%">&nbsp;</td>
<td style="width:33.3333%"><strong>Column I</strong></td>
<td style="width:33.3333%"><strong>Column II</strong></td>
</tr>
<tr style="display:flex;flex-direction:row;">
<td style="width:33.3333%"><strong>(i)</strong></td>
<td style="width:33.3333%"e for living organisms</td>
<td style="width:33.3333%">(b) all kind of soil</td>
</tr>
<tr style="display:flex;flex-direction:row;">
<td style="width:33.3333%"><strong>(ii)</strong></td>
<td style="width:33.3333%">(ii) Upper layer of the soil</td>
<td style="width:33.3333%">(c) Dark in color</td>
</tr>
<tr style="display:flex;flex-direction:row;">
<td style="width:33.3333%"><strong>(iii)</strong></td>
<td style="width:33.3333%">(iii) Sandy soil</td>
<td style="width:33.3333%">(a) Large particles</td>
</tr>
<tr style="display:flex;flex-direction:row;">
<td style="width:33.3333%"><strong>(iv)</strong></td>
<td style="width:33.3333%">(iv) Middle layer of soil</td>
<td style="width:33.3333%">(e) lesser amount of humus</td>
</tr>
<tr style="display:flex;flex-direction:row;">
<td style="width:33.3333%">(v)</td>
<td style="width:33.3333%">(v) Clayey soil</td>
<td style="width:33.3333%">(d) Small particles and packed tight</td>
</tr>
</tbody>
</table>
</br>(38) The various component of soil are – Rock particles, Minerals, Humus, Air, Water and Living organisms.

Humus of soil will float on the surface of water.

</br>(39) Volume of water percolated= 180 mL

Time taken for percolation= 45 minutes

Percolation rate = 180 mL / 45 min = 4 mL/min.

</br>(40) If there is vegetation (trees and other plants) on land then the roots of vegetation growing in the soil bind the particles of top soil firmly. For this reason binding of soil particles by the roots of trees and plants, the blowing wind and flowing rainwater are not able to carry away top soil and hence soil erosion does not occur.

Whereas the bare soil is eroded easily as because there is totally negative from the upper mentioned.

</br>(41) There are number of points by which trees and other plants prevent soil erosion:-

</br>(i) By preventing large scale cutting down of forest trees.

</br>(ii) By afforestatation..

</br>(iii) By increasing the green cover around us by planting more trees and plants our selves.

</br>(42) Soil erosion can be prevented by:-

</br>(i) By preventing large scale cutting down of forest trees.

</br>(b) Soil erosion can be prevented by large scale growing of forest trees in place of c0ut down trees.

</br>(c) Soil erosion can be prevented by increasing the green cover around us by planting more trees and plants our selves.

</br>(43) The difference between sandy soil and clayey soil are :-

</br>(i) sandy soil contains big rock particles whereas clayey soil contains fine rock particles.

</br>(ii) Sandy soil cannot hold much water whereas clayey soil hold much water.

</br>(iii) Sandy soil is loose, light and non sticky whereas clayey soil is compact, heavy and sticky.

</br>(44) The important use of soil:-

</br>(i) Soil is used for growing food.

</br>(ii) It is used to grow trees for obtaining wood for building purposes, for burning as fuel, and for making paper.

</br>(iii) Soil is used to make bricks and mortar for building houses.

</br>(45) It is because they cause soil pollution and may also kill the living organisms.
<h3><span style="color: #ff0000;"><strong>Large Answer Type Questions Answers:</strong></span></h3>
</br>(46) <strong>Soil Profile:-</strong> A vertical section through the soil showing the different layers of soil is called soil profile.

<strong>Two situation in which we can see the soil profile :-</strong>

</br>(a) By looking at the sides of a recently dug ditch.

</br>(b) While digging a well or foundation of a building.

</br>(47) <strong><u>Sandy Soil:- </u></strong>

Sandy soil contains mainly sand.

<strong><u>Two properties of sandy soil:- </u></strong>

</br>(i) Sandy soil contains very little humus.

</br>(ii) Sandy soil is less fertile.

</br><u><strong>Clayey soil:-</strong> </u>Clayey soil contains mainly clay (having very small particles with very small spaces).

</br><strong><u>Two properties of clayey soil:-</u></strong>

</br>(i) Clayey soil also contains very little humus.

</br>(ii) Clayey soil is more fertile than sandy soil.

</br><strong><u>Loamy soil:- </u> </strong>Loamy soil is a mixture of sand, clay, silt, and humus in the right proportions.

</br><strong><u>Two properties of loamy soil:-  </u></strong>

</br>(i) Loamy  soil contains sufficient amount of humus.

</br>(ii) Loamy soil is the fertile soil.

</br>(48) <strong><u>Soil:-</u></strong> Soil is the most important natural resources. Soil is essential for the existence of life on the earth.

</br><strong><u>How Soil is Formed:</u></strong>

Soil is formed from rocks by the process of weathering. In weathering rocks are broken down by the action of suns heat, wind, rain etc. to form tiny rock particles.

This tiny rock particles mixes with humus to form fertile soil.

<strong><u>An activity showed soil contains water</u>:-</strong>

Take some dry soil in a beaker and cover it by watch glass. Now burn the beaker some times, you can see little drop of water inside the surface of beaker.

This activity shows us that the soil which looks like dry contains with water.

</br>(49) <strong><u>Soil Erosion:- </u></strong>

Removal of fertile top soil from land by wind or water is called soil erosion.

</br><strong><u>Cause of Soil Erosion:-</u></strong>

The main cause of soil erosion is the large scale cutting down of forest trees.

</br><strong><u>Effects of Soil Erosion:-</u></strong>

</br>The effects of soil erosion are following:-

</br>(i) Soil erosion can turn lush green forests into deserts and spoil the environment.

</br>(ii) This can lead to famines.

</br>(iii) It can also flood.

</br>(50) <u><strong>Soil Pollution:-</strong>  </u>The mixed of soil with waste materials such as polythene bags, glass and metal objects called soil pollution.

</br><strong><u>Cause of Soil Pollution:- </u></strong>

</br>There various cause of soil pollutions. Some of are:- dumping of waste materials, Use of pesticides in agriculture, Use of fertilizers in agriculture and many more.

</br><strong><u>Prevention of Soil Pollution:- </u></strong>

</br>(i) The use of polythene bags should be avoided instead of we have to use the bags made of jute, paper, cotton cloth etc.

</br>(ii) The use of pesticides in agricultural activity should be minimize instead of we can use manure (natural fertilizer).

</br>(iii) The industrial waste should be treated so that no harm will spread. Etc.</div>`;
export default htmlContent;