const htmlContent =`<h1><span style="color: #000080;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 18th Chapter <span style="color: #ff0000;">Waste Water Story</span> Solution</strong></span></h1>
Lakhmir Singh and Manjit Kaur Science solution: Waste Water Story Chapter 18. Here you get easy solutions of Lakhmir Singh and Manjit Kaur Science solution Chapter 18. Here we give Chapter 18 all solution of class 7. Its help you to complete your homework.

Board – CBSE

Text Book - SCIENCE

<span style="color: #ff0000;"><strong>Class – 7</strong></span></br>

<span style="color: #333399;"><strong>Chapter – 8</strong></span>
<h3><span style="color: #ff0000;"><strong>Very Short Answer Type Questions:</strong></span></h3>
<div>(1) Human Faeces, Animal waste.

</br>(2) Nitrates, Phosphates.

</br>(3) Nitrogen , Phosphorous.

</br>(4) Bacteria.

</br>(5) Cholera, Typhoid.

</br>(6) Dysentery.

</br>(7) Waste Water Treatment plant.

</br>(8) Chlorine and Ozone.

</br>(9) Sludge.

</br>(10) Biogas and Manure.

</br>(11) Water.

</br>(12) Cholera, Typhoid, Dysentery, Polio.

</br>(13) True

</br>(14) Setic Tank, Composting Pits.
</br>(15) Septic Tank toilets.

</br>(16) Chemical toilets.

</br>(17) Composting Toilets.

</br>(18) Vaccum Toilets.

</br>(19) (a) anaerobic bacteria (b) aerobic bacteria.

</br>(20) (a) Sewage

</br>(b) Pollutants

</br>(c) Sludge

</br>(d) solid food remains; sanitary towels.

</br>(e) health

</br>(f) soil; water

</br>(g) earthworms

</br>(h) chemical

</br>(i) diseases.
<h3><span style="color: #ff0000;"><strong>Short Answer Type Questions:</strong></span></h3>
</br>(21) (a) An underground pipe which carries away dirty drainage water and waste matter is called a sewer.

</br>(b) Sewerage is an underground network of interconnected pipes that transport or carries the sewage from the place where it is produced to the sewage treatment plant where it is processed.

</br>(22) The bar screen removes the large rubbish objects like rags, sticks, cans, plastic bags, napki8ns, sanitary towels etc.

</br>(23) The solid part of sewage is called a sludge.

The sludge is taken out from the bottom of first sedimentation tank and put into a large, closed tank called digester tank. In the digester tank many type of anaerobic bacteria decompose the organic matter present in sludge to produce biogas.

</br>(24) Two useful substances are Biogas and Sludge.

The biogas is used as fuel. Sludge is used as Manure.

</br>(25) This is because to provide oxygen to activate aerobic bacteria and make them grow rapidly in water.

</br>(26) The waste such as solid food remains, used tea leaves, sanitary towels, polythene bags, oils and fats, as well as chemicals such as paints, solvents, medicines etc as they are block the drainage pipe.

</br>(27) Oils and Fats should not be released in the drain because this can harden and block the drainage pipes. As well as in an open drain the fats block the soil pores reducing its effectiveness in filtering water.

</br>(28) Vermi processing toilet is those type of toilet in where human excreta is treated by earthworms in a pit.

Vermi cakes produced by this type of toilets are a kind of high quality manure which can be added to soil for growing plants.

</br>(29) Then the epidemics could break out.

</br>(30) (i) We should not scatter rubbish objects such as paper, food wastes, packets, water bottles etc in public places.

</br>(ii) Don’t spit in public places.

</br>(iii) We should never urinate on the road side.
<h3><span style="color: #ff0000;"><strong>Long answer Type Questions:</strong></span></h3>
</br>(31) The wastewater and faeces from homes and other buildings which is carried away in sewers is called sewage.

When we washes our clothes , cleans our utensils , takes baths , goes to washroom in all this process the water used becomes contaminated and becomes sewage.

The pipes are called Sewers.

</br>(32) (a) This is because the discharge of untreated sewage into rivers and sea kill fish and other aquatic animals.

</br>(b) The processing of sewage to remove pollutants and make it harmless so that it may be disposed of safely in the rivers etc is called sewage treatment plant.

The main purpose of this is to reduce the pollutants present in it to such a low level that it may not remain a health hazard for any one.

</br>(33) The various steps involved in getting clear water from wastewater are:- (i) Screening (ii) Grit and Sand Removal (iii) First Sedimentation Tank (iv) Aeration Tank (v) Second Sedimentation Tank.

Biogas is extracted during this process.

</br>(34) Sanitation means cleans environment which is essential for ours and for our future generation. Poor sanitation create a lot of diseases in our country.

</br><strong>The various ways we can contribution maintaining sanitation at public places are:-</strong>

</br>(i) We should not scatter rubbish objects such as paper, food wastes, packets, water bottles etc in public places.

</br>(ii) Don’t spit in public places.

</br>(iii) We should never urinate on the road side.

</br>(35) Septic Tank:- is a big, covered, underground tank made of concrete having an inlet pipe at one end and an outlet pipe in other end.

<strong>Describe the septic tank based toilet with help of lebelled diagram:- Please check the book.</strong>

Septic tank toilets are used in those places where there is no sewerage system.</div>
<p style="text-align: center;"><strong>_____ x ____</strong></p>`;
export default htmlContent;