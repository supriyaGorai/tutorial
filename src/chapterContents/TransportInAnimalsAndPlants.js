const htmlContent =`<h1><span style="color: #000080;"><strong>Lakhmir Singh Manjit Kaur Class 7 Science 11th Chapter <span style="color: #00ff00;">Transport in Animals And Plants</span> Solution</strong></span></h1>
Lakhmir Singh and Manjit Kaur Science solution: Transport in Animals And Plants Chapter 11. Here you get easy solutions of Lakhmir Singh and Manjit Kaur Science solution Chapter 11. Here we give Chapter 11 all solution of class 7. Its help you to complete your homework.

Board – CBSE

Text Book - SCIENCE

Class – 7

Chapter – 11
<h2><span style="color: #ff0000;"><strong>Very Short answer Type Questions Answers</strong></span></h2>
<div><strong>(1) Platelets help in the clotting of blood in a cut or wound.</strong></br>

<strong>(2) Plasma.</strong></br>

<strong>(3) White Blood Cells (WBC).</strong></br>

<strong>(4) (a) Red Blood Cells</strong></br>

<strong>(b)  Haemoglobin.</strong></br>

<strong>(5) Haemoglobin.</strong></br>

<strong>(6) RBC carry oxygen from the lungs to all the cells of the body.</strong></br>

<strong>(7) White Blood Cells.</strong></br>

<strong>(8) Plasma.</strong></br>

<strong>(9) Due to the haemoglobin the color of blood become red.</strong></br>

<strong>(10) (a) Oxygen</strong></br>

<strong>(b) Carbon Di Oxide.</strong></br>

<strong>(11) (a) RBC = Red Blood Cells.</strong></br>

<strong>(b) WBC = White Blood Cells.</strong></br>

<strong>(12) Heart.</strong></br>

<strong>(13) Veins.</strong></br>

<strong>(14) Arteries.</strong></br>

<strong>(15) Capillaries.</strong></br>

<strong>(16) (a) arteries</strong></br>

<strong>(b) Veins.</strong></br>

<strong>(17) (a) Right side</strong></br>

<strong>(b) Left side.</strong></br>

<strong>(18) Right side</strong></br>

<strong>(b) Left side</strong></br>

<strong>(19) Lungs.</strong></br>

<strong>(20) 95</strong></br>

<strong>(21) Artery</strong></br>

<strong>(22) Amoeba, Paramecium.</strong></br>

<strong>(23) Kidneys</strong></br>

<strong>(24) (a) Carbon Di Oxide</strong></br>

<strong>(b) Urea</strong></br>

<strong>(25) (a) Lungs</strong></br>

<strong>(b) Kidneys</strong></br>

<strong>(26) Urea is removed from the blood by Kidneys.</strong></br>

<strong>(27) The percentage of composition of urine is – 2.5% Urea, 2.5% other waste salts and 95% water.</strong></br>

<strong>(28) Dialysis.</strong></br>

<strong>(29) (a) Xylem.</strong></br>

<strong>(b) Phloem.</strong></br>

<strong>(30) (a) Red</strong></br>

<strong>(b) Capillaries</strong></br>

<strong>(c) heart</strong></br>

<strong>(d) Four</strong></br>

<strong>(e) William Harvay.</strong></br>

<strong>(f) Heartbeat</strong></br>

<strong>(g) Urea</strong></br>

<strong>(h) Ureters</strong></br>

<strong>(i) Urethra</strong></br>

<strong>(j) Nephrons</strong></br>

<strong>(k) Cells</strong></br>

<strong>(l) Root hair</strong></br>

<strong>(m) Xylem</strong></br>

<strong>(n) Xylem</strong></br>

<strong>(o) Phloem</strong></br>

<strong>(p) transpiration.</strong></br>
<h2><span style="color: #ff0000;"><strong>Short Answer Type Questions Answers:</strong></span></h2>
<strong>(31) This is needed to supply them with food, water and oxygen and to carry away the harmful waste materials produced in their bodies.</strong></br>

<strong>(32) Blood is the red coloured liquid which flows in blood vessels and circulates in our body.</strong>

<strong>The components of blood are Plasma, Red Blood Cells, White Blood Cells, and Platelets.</strong>

<strong>Haemoglobin makes the red coloured.</strong></br>

<strong>(33) Blood is needed by all the parts of the body to carries digested food, water to all the parts of the body, oxygen from the lungs to different parts of the body.</strong></br>

<strong>(34) Then bleeding caused by a cut from an injury would not stop.</strong></br>

<strong>(35) The function of blood are following:-</strong></br>

<strong>(a) It carries digested food from the small intestine to all the parts of the body.</strong></br>

<strong>(b) It carries blood to all the parts of the body.</strong></br>

<strong>(c) It carries oxygen from the lungs to different parts of the body.</strong></br>

<strong>(d) It carries urea from the liver to kidneys for excretion in urine.</strong></br>

<strong>(36) The heart is an organ which pumps blood to all the parts of our body.</strong></br></br>

<strong><u>Function of Heart:-</u></strong>

Keep the oxygen rich blood separate so that it may supply good amount of oxygen to the body cells for respiration and release of energy.

</br>(37) There are Four chambers in the heart.

</br>(a) The upper chamber of heart is called atria.

</br>(b) The lower chamber of heart is called ventricles.

</br>(38) There are three types of blood vessels in the body. They are Arteries, Veins and Capillaries.

</br>(39) Arteries:- Arteries are the blood vessels which carry blood from the heart to all the parts of the body.

Veins:- are the blood vessels which carry blood from all the parts of the body back to the heart.

Capillaries:-  capillaries are the extremely thin blood vessels which connect arteries to veins.

</br>(40) By the help of blood food and oxygen reach all the parts of the body.

</br>(41) One complete contraction and relaxation of the heart is called heartbeat.

Stethoscope used by doctors to listen to our heartbeat.

</br>(42) The average number of heartbeat is 72 – 80 times while resting.

The heartbeat faster during and after exercise for the reason is our body needs more energy under these condition.

</br>(43) An instrument by which a doctor listens  to our heartbeats is called stethoscope.

The various parts of stethoscope are:- Ear pieces, Rubber tube, Chest piece, Diaphragm.

</br>(44) The expansion of an artery each time the blood is forced into it is called pulse.

We can feel the pulse in the wrist.

The pulse rate of an adult person is 72 – 80 times while resting.

</br>(45) We feel the pulse by placing first two fingers gently on arteries at the wrist.

A small baby has a high pulse rate.

</br>(46) Hydra, Sponges, Amoeba, Paramecium do not have blood circulatory system.

</br>(47) It is the water by which food, oxygen, carbon dioxide and other waste materials transported in animals such as Sponges and Hydra.

</br>(48)
<table>
<tbody>
<tr>
<td width="213"><strong>Column I</strong></td>
<td width="213"><strong>Column II</strong></td>
</tr>
<tr>
<td width="213">(i) Stomata</td>
<td width="213">(b) Transpiration</td>
</tr>
<tr>
<td width="213">(ii) Xylem</td>
<td width="213">(d) Transport of water</td>
</tr>
<tr>
<td width="213">(iii) Root hair</td>
<td width="213">(a) Absorption of water</td>
</tr>
<tr>
<td width="213">(iv) Phloem</td>
<td width="213">(c) Transport of food</td>
</tr>
</tbody>
</table>
</br>(49) The procedure used for cleaning the blood of a person by separating the waste product urea from it is called dialysis.

The patient whose kidney are failure put in dialysis machine.

</br>(50) If the kidneys stop working then artificial kidney which is a machine is used to remove urea and other waste salts from the blood of a person by filtering it.

The best solution is kidney transplant.

</br>(51) A person having kidney failure survive by kidney transplanted. In kidney transplant the damaged kidney is removed and a matching kidney donated by a healthy person is transplanted in its place by performing a surgical operation.

</br>(52) Sweat contains water, some salts and a little of urea.

Function of Sweat: Sweat is removed from the body by sweat glands  through the skin.

</br>(53) This white patches are formed by the salts left on the clothes when the water present in sweat evaporates.

</br>(54) (a) The aquatic animals like fish excrete their cell waste.

</br>(b) The land animals such as birds, lizards, snakes and insects excrete a white coloured waste called uric acid in semi solid form.

</br>(55) (a) A tissue is a group of similar cells which work together to perform a particular function.

</br>(b) Those tissues which transport water, minerals and food to different parts of a plant are called vascular tissues.

The two types of vascular tissues are Xylem and Phloem.

</br>(56) One of tissues which carries water and minerals from the roots to the leaves of a plant called Xylem.

One of tissue which carries food from the leaves to other parts of the plants is called phloem.

<strong><u>Function of Xylem &amp; Phloem:</u></strong></br></br>

<strong>*</strong> Xylem carries water and minerals from the roots to the leaves of a plant.

<strong>* </strong>Phloem carries water and minerals from the roots to the leaves of a plant.

</br>(57) The name of process is Transpiration.

Water moves from the roots only upwards but food moves from the leaves upwards as well as downwards.

</br>(58) Yes, Transpiration serve useful function in the plants.

</br>(i) To carry water absorbed by the roots up to the leaves.

</br>(ii) To carry the food made in the leaves to all the parts of the plant.

</br>(59) Stomata are the tiny pores found on the surface of leaves of plants.

</br>The function of stomata are following:-

</br>(i) CO<sub>2</sub> gas enters in and O<sub>2</sub> gas goes out through stomata during photosynthesis.

</br>(ii) Water vapour passes out through stomata during transpiration.

</br>(60) (a) Through Xylem water and mineral passes out from the roots to the leaves of a plant.

</br>(b) Through Phloem food move from the leaves to all the other parts of a plant.
<h2><span style="color: #ff0000;"><strong>Long Answer Type Questions Answers:</strong></span></h2>
</br>(61) <u>Function of Circulatory System:-</u>  The blood circulatory system makes food, water and oxygen available to every part of the body and helps in removing waste materials of the body like carbon di oxide etc.

</br>The various parts of human circulatory system are:-

</br>(a) Heart (b) Blood (c) Blood Vessels .

</br>(62) Excretion:- The removal of waste materials produced in the cells of the living organisms is called excretion.

The various waste material which are excreted from the human beings are Carbon Di Oxide, Urea and Sweat.

It is necessary to excrete waste product because are following:-

All cells of our body produce waste products. These waste products are toxic to the body and therefore need to be excreted out. This process of removing waste products produced in the cells of living organisms is called excretion.

</br>(63) The various organs of human excretory system are:-

</br>(i) Two Kidneys

</br>(ii) Two ureters

</br>(iii) A bladder

</br>(iv) A urethra

</br>(64) Plants need transport system:

</br>(i) To carry water, absorbed by the roots to the leaves.

</br>(ii) To carry food made in the leaves to all the parts of the plants.

The two types of vessels are Xylem and Phloem.

</br>(65) The movement of water takes place from the area of high water potential to the area of low water potential by osmosis.

The roots always have less water potential as compared to the soil. Thus, water moves easily from soil to root xylem through root hairs by the process of osmosis.

Flowchart of movement of water is as follows:

Root hair → Epidermis→ Cortex → Endodermis → Root xylem → Stem xylem.</div>`;
export default htmlContent;