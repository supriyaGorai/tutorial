const TAG_STYLE = {
  table: {
    margin: 10,
    flex: 1,
    flexDirection: 'row',
    borderLeftWidth: 1,
    borderTopWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#ccc',
    borderRightWidth: 0.5,
  },
  tbody: {},
  tr: {
    flex: 1,
    borderBottomWidth: 1,
    flexDirection: 'row',
    borderBottomColor: '#ccc',
  },
  td: {
    justifyContent: 'flex-start',
    width: '50%',
    paddingHorizontal: 12,
    paddingVertical: 5,
    borderRightWidth: 0.5,
    borderRightColor: '#ccc',
    alignItems: 'flex-start',
  },
  div:{
    fontSize:18
  },
  p:{
    fontSize:18
  }
};

export default TAG_STYLE;
