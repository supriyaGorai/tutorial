import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  StatusBar,
  ScrollView,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import HTML from 'react-native-render-html';
import IGNORED_TAGS from './../webViewTable/ignoredTags';
import TAG_STYLE from './../webViewTable/tagStyles';
import htmlContent from './../chapterContents/ElectricCurrents';

class ElectricCurrents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
    setTimeout(() => {
      this.setState({isLoading: false});
    }, 100);
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <StatusBar barStyle="dark-content" />
        {this.state.isLoading ? (
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator size="large" color="#3F51B5" />
          </View>
        ) : (
          <ScrollView style={{flex: 1}}>
            <View style={styles.container}>
              <View style={styles.veryShortAnswerView}>
                <HTML
                  html={htmlContent}
                  tagsStyles={TAG_STYLE}
                  ignoredTags={IGNORED_TAGS}
                  imagesMaxWidth={Dimensions.get('window').width}
                />
              </View>
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  veryShortAnswerView: {
    padding: 10,
  },
});

export default ElectricCurrents;
